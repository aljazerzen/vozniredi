package si.erzen.aljaz.vozniredi.backend;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.TreeMap;

public class Stations extends TreeMap<Long, Stations.Station> {

	private static final String filename = "stations.ijpp.json";

	private static Stations singleton = null;

	public static Stations getInstance(Context context) {
		if (singleton == null)
			singleton = loadStations(context);
		return singleton;
	}

	private static Stations loadStations(Context context) {

		// read json data from file
		String jsonString = "";
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(context.getAssets().open(filename)));
			StringBuilder sb = new StringBuilder();

			String mLine;
			while ((mLine = reader.readLine()) != null) {
				sb.append(mLine);
			}
			jsonString = sb.toString();
		} catch (IOException ignored) {
		} finally {
			if (reader != null)
				try {
					reader.close();
				} catch (IOException ignored) {
				}
		}

		Stations list = new Stations();
		try {
			JSONArray jsonArray = new JSONArray(jsonString);
			for (int i = 0, l = jsonArray.length(); i < l; i++) {
				JSONObject o = jsonArray.getJSONObject(i);

				String label = o.getString("label");
				int mod = Integer.parseInt(o.getString("mod"));
				String naselje = o.getString("naselje");
				String obcina = o.getString("obcina");
				// String tip = o.getString("tip");
				long id = Long.parseLong(o.getString("value"));


				if (mod == 2) {
					list.put(id, new Station(label, naselje, obcina, id));
				}
			}
		} catch (JSONException ignored) {

		}
		return list;
	}

	public static class Station {
		String label;
		String naselje;
		String obcina;
		long id;

		public String getLabel() {
			return label;
		}

		public String getNaselje() {
			return naselje;
		}

		public String getObcina() {
			return obcina;
		}

		public long getId() {
			return id;
		}

		Station(String label, String naselje, String obcina, long id) {
			this.label = label;
			this.naselje = naselje;
			this.obcina = obcina;
			this.id = id;
		}
	}
}
