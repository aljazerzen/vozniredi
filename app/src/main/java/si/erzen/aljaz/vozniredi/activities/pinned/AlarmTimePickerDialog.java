package si.erzen.aljaz.vozniredi.activities.pinned;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import si.erzen.aljaz.vozniredi.R;

import static android.app.Activity.RESULT_OK;
import static si.erzen.aljaz.vozniredi.util.Constants.ARG_ALARM_MIN;
import static si.erzen.aljaz.vozniredi.util.Constants.ARG_PINNED_ID;

public class AlarmTimePickerDialog extends DialogFragment implements View.OnClickListener {
	public static final int REQUEST_CODE = 0x21ad;

	public static AlarmTimePickerDialog getInstance(long pinnedDriveId) {
		AlarmTimePickerDialog dialog = new AlarmTimePickerDialog();
		Bundle args = new Bundle();
		args.putLong(ARG_PINNED_ID, pinnedDriveId);
		dialog.setArguments(args);
		return dialog;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		setStyle(DialogFragment.STYLE_NO_TITLE, R.style.AppTheme);

		View rootView = inflater.inflate(R.layout.dialog_alarm_time_picker, container, false);
		rootView.findViewById(R.id.button_set_alarm).setOnClickListener(this);
		rootView.findViewById(R.id.button_cancel).setOnClickListener(this);
		rootView.findViewById(R.id.button_alarm_add15).setOnClickListener(this);
		rootView.findViewById(R.id.button_alarm_add5).setOnClickListener(this);
		rootView.findViewById(R.id.button_alarm_sub15).setOnClickListener(this);
		rootView.findViewById(R.id.button_alarm_sub5).setOnClickListener(this);
		return rootView;
	}

	@SuppressLint("SetTextI18n")
	@Override
	public void onClick(View view) {
		int dMin = 0;
		switch (view.getId()) {
			case R.id.button_set_alarm:
				View rootView = getView();

				if (rootView != null) {

					int min = Integer.parseInt(((EditText) rootView.findViewById(R.id.alarm_min_input)).getText().toString());

					Intent intent = new Intent();
					intent.putExtra(ARG_ALARM_MIN, min);
					intent.putExtras(getArguments());
					getTargetFragment().onActivityResult(REQUEST_CODE, RESULT_OK, intent);
				}
				dismiss();
				break;
			case R.id.button_cancel:
				dismiss();
				break;
			case R.id.button_alarm_add15:
				dMin = 15;
			case R.id.button_alarm_add5:
				dMin = dMin == 0 ? 5 : dMin;
			case R.id.button_alarm_sub15:
				dMin = dMin == 0 ? -15 : dMin;
			case R.id.button_alarm_sub5:
				dMin = dMin == 0 ? -5 : dMin;
				rootView = getView();

				if (rootView != null) {
					EditText editText = ((EditText) rootView.findViewById(R.id.alarm_min_input));
					int min = Integer.parseInt(editText.getText().toString());
					min = Math.max(0, min + dMin);
					editText.setText(Integer.toString(min));
				}
				break;
		}
	}
}
