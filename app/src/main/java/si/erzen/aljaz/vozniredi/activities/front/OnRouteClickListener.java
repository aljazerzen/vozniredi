package si.erzen.aljaz.vozniredi.activities.front;

import si.erzen.aljaz.vozniredi.backend.Schedule;

interface OnRouteClickListener {
	void onRouteInvoke(Schedule route);

	void onRouteDelete(Schedule route, int position);
}
