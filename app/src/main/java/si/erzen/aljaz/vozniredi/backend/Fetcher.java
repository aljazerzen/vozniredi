package si.erzen.aljaz.vozniredi.backend;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_ARRIVAL;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_DEPARTURE;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_OPERATOR;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_REGIME;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_REGIME_LONG;

public class Fetcher {

	private static Fetcher singleton = null;

	private RequestQueue requestQueue;
	private Database database;

	private Fetcher(Context context) {
		database = Database.getInstance(context);
		requestQueue = Volley.newRequestQueue(context);
	}

	public static synchronized Fetcher getInstance(Context context) {
		if (singleton == null)
			singleton = new Fetcher(context);
		return singleton;
	}

	public void fetchSchedule(Schedule schedule, final ScheduleListener scheduleListener) {
		ScheduleListener listener = new ScheduleListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				scheduleListener.onErrorResponse(error);
			}

			@Override
			public void onResponse(Schedule response) {
				database.save(response);
				scheduleListener.onResponse(response);
			}
		};
		requestQueue.add(new RequestIJPP(schedule, listener));
	}

	private static class RequestIJPP extends Request<Schedule> {

		static final Map<String, Integer> providers;

		static {
			providers = new TreeMap<>();
			providers.put("ALPETOUR - POTOVALNA AGENCIJA, D.O.O.", 0);
			providers.put("AP NOVAK D.O.O.", 1);
			providers.put("ARRIVA DOLENJSKA IN PRIMORSKA D.O.O.", 2);
			providers.put("ARRIVA ŠTAJERSKA D.D.", 3);
			providers.put("AVRIGO D.O.O.", 4);
			providers.put("AVTOBUSNI PREVOZI KASTIGAR POLIKARP S.P.", 5);
			providers.put("AVTOBUSNI PREVOZI LIPIČNIK ANITA S.P.", 6);
			providers.put("AVTOBUSNI PREVOZI MRGOLE D.O.O.", 7);
			providers.put("AVTOBUSNI PREVOZI NOVAK BUS D.O.O.", 8);
			providers.put("AVTOBUSNI PREVOZI RIŽANA D.O.O.", 9);
			providers.put("AVTOBUSNI PROMET MURSKA SOBOTA D.D.", 10);
			providers.put("AVTOPREVOZNIŠTVO KRAŠEVEC SANDI S.P.", 11);
			providers.put("AVTOPREVOZNIŠTVO MARTIN - VALENTINA FRELIH S.P.", 12);
			providers.put("AVTOPREVOZNIŠTVO ŠAMU TURS S.P.", 13);
			providers.put("FS PREVOZI, STORITVE IN TRGOVINA D.O.O.", 14);
			providers.put("INTEGRAL AP TRŽIČ D.O.O. - SKUPINA ALPETOUR POTOVALNA AGENCIJA", 15);
			providers.put("INTEGRAL BREBUS BREŽICE D.O.O.", 16);
			providers.put("INTEGRAL NOTRANJSKA D.O.O.", 17);
			providers.put("INTEGRAL STOJNA AVTOBUSNI PROMET D.O.O.", 18);
			providers.put("INTEGRAL VOZNIK D.O.O.", 19);
			providers.put("INTEGRAL ZAGORJE D.O.O.", 20);
			providers.put("IZLETNIK CELJE D.O.O.", 21);
			providers.put("JAVNO PODJETJE LJUBLJANSKI POTNIŠKI PROMET, D.O.O.", 22);
			providers.put("JAVNO PODJETJE ZA MESTNI POTNIŠKI PROMET MARPROM D.O.O.", 23);
			providers.put("KAM-BUS D.O.O. - SKUPINA ALPETOUR POTOVALNA AGENCIJA", 24);
			providers.put("KORATUR D.D.", 25);
			providers.put("MIRTTOURS PETETR MIRT S.P.", 26);
			providers.put("MPOV D.O.O. VINICA", 27);
			providers.put("OŠTRMAN TOURS PAVEL ŠTERK S.P.", 28);
			providers.put("POHORJE TURIZEM, AVTOBUSNI PREVOZI IN TURIZEM, D.O.O.", 29);
			providers.put("PREVOZI PRIJATELJ D.O.O.", 30);
			providers.put("PREVOZI ROK JAKLIČ S.P.", 31);
			providers.put("SŽ-ŽIP, STORITVE, D.O.O.", 32);
			providers.put("ŠPIK MIROSLAV ŠPIK S.P. PREVOZ OSEB-APARTMAJI", 33);
			providers.put("TEHNOJUR D.O.O.", 34);
			providers.put("ZGORNJI ZAVRATNIK D.O.O.", 35);
		}

		static final String address = "http://www.jpp.si/portalJPP/srvIzvediSql";

		private Map<String, String> mParams;
		private ScheduleListener onResponseListener;
		private Schedule requestedSchedule;

		RequestIJPP(Schedule schedule, ScheduleListener scheduleListener) {
			super(Method.POST, address, scheduleListener);
			onResponseListener = scheduleListener;
			requestedSchedule = schedule;

			mParams = new HashMap<>();
			mParams.put("POSTAJALISCE_ID_OD", "" + schedule.fromId);
			mParams.put("POSTAJALISCE_ID_DO", "" + schedule.toId);
			mParams.put("TIP_OD", "postaja");
			mParams.put("TIP_DO", "postaja");
			mParams.put("DATUM", schedule.date != null ? schedule.date.replace("-", "") : "");
			mParams.put("PREVOZNIK", "0");
			mParams.put("akcija", "vrniRelacijskiVozniRed");
		}

		@Override
		public Map<String, String> getParams() {
			return mParams;
		}

		@Override
		public Map<String, String> getHeaders() throws AuthFailureError {
			Map<String, String> headers = new TreeMap<>();
			headers.put("Referer", "http://www.jpp.si/web/guest/pregledovalnik");
			return headers;
		}

		@Override
		protected Response<Schedule> parseNetworkResponse(NetworkResponse response) {
			JSONArray formattedArray = new JSONArray();
			Log.d("fetcher", "response: " + new String(response.data));
			try {
				JSONObject responseRoot = new JSONObject(new String(response.data));
				JSONObject podatki = responseRoot.getJSONObject("podatki");

				Iterator<String> itr = responseRoot.getJSONObject("doPostajalisca").keys();
				if (itr.hasNext()) {
					JSONArray responseArray = podatki.getJSONArray("" + itr.next());
					for (int i = 0, l = responseArray.length(); i < l; i++) {
						JSONObject resObj = responseArray.getJSONObject(i);

						if (resObj.getString("modaliteta").equals("bus")) {
							JSONObject formattedObj = new JSONObject();
							formattedObj.put(FIELD_ARRIVAL, resObj.getString("arrival_time"));
							formattedObj.put(FIELD_DEPARTURE, resObj.getString("departure_time"));
							String operator = "" + providers.get(resObj.getString("prevoznik"));
							if (operator.equals("null")) {
								operator = resObj.getString("prevoznik").split(" [.-]|(D.)|(S.)")[0];
							}
							formattedObj.put(FIELD_OPERATOR, operator);

							if (requestedSchedule.isGeneral()) {
								formattedObj.put(FIELD_REGIME, resObj.getString("oznaka_rezima"));
								formattedObj.put(FIELD_REGIME_LONG, resObj.getString("opis_rezima"));
							}

							formattedArray.put(formattedObj);
						}
					}
				}
			} catch (JSONException e) {
				Log.e("fetcher", "response: " + new String(response.data));
				Log.e("fetcher", "JSON parse error: " + e.toString());
				return Response.error(new ParseError(response));
			}
			requestedSchedule.data = formattedArray.toString();
			requestedSchedule.lastUpdate = System.currentTimeMillis();
			return Response.success(requestedSchedule, getCacheEntry());
		}

		@Override
		protected VolleyError parseNetworkError(VolleyError volleyError) {
			try {
				Log.e("fetcher", new String(volleyError.networkResponse.data));
			} catch (NullPointerException ignored) {
			}
			return super.parseNetworkError(volleyError);
		}

		@Override
		protected void deliverResponse(Schedule schedule) {
			onResponseListener.onResponse(schedule);
		}
	}

	public interface ScheduleListener extends Response.Listener<Schedule>, Response.ErrorListener {
	}
}
