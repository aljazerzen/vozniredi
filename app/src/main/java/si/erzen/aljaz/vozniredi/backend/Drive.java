package si.erzen.aljaz.vozniredi.backend;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;
import java.util.TreeMap;

import si.erzen.aljaz.vozniredi.util.Formatter;

import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_ARRIVAL;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_DEPARTURE;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_OPERATOR;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_REGIME;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_REGIME_LONG;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_TTD;

public class Drive extends TreeMap<String, String> implements ScheduleDataSetItem {

	public static final String[] FIELDS_TO_SAVE = new String[]{FIELD_DEPARTURE, FIELD_ARRIVAL, FIELD_OPERATOR, FIELD_REGIME, FIELD_REGIME_LONG};

	Drive(JSONObject jsonObject) {
		Iterator<String> it = jsonObject.keys();
		while (it.hasNext()) {
			String key = it.next();
			try {
				put(key, jsonObject.getString(key));
			} catch (JSONException ignored) {
			}
		}
	}

	Drive addTTDField(String date) {
		long difference = 0;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
			format.setTimeZone(TimeZone.getTimeZone("Europe/Ljubljana"));
			difference = format.parse(date + " " + get(FIELD_DEPARTURE)).getTime();
			difference -= Calendar.getInstance().getTimeInMillis();
			if (difference < 0) {
				difference = 0;
			}
		} catch (ParseException ignored) {
		}

		String ttd;
		difference /= 1000 * 60;
		if (difference == 0 || difference >= 60 * 48) {
			ttd = "--:--";
		} else {
			ttd = String.format(Locale.ENGLISH, "%02d:%02d", (difference / 60), (difference % 60));
		}

		put(FIELD_TTD, ttd);
		return this;
	}

	public String getDuration(Context context) {
		return Formatter.ISOTimeToHumanElapsed(get(FIELD_DEPARTURE), get(FIELD_ARRIVAL), context);
	}

	@Override
	public String toString() {
		JSONObject jsonObject = new JSONObject();

		for (String field : FIELDS_TO_SAVE) {
			if (containsKey(field)) {
				try {
					jsonObject.put(field, "" + get(field));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return jsonObject.toString();
	}
}
