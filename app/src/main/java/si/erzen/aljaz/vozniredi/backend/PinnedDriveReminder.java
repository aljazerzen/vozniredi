package si.erzen.aljaz.vozniredi.backend;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_ARRIVAL;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_DEPARTURE;

public class PinnedDriveReminder implements ScheduleDataSetItem, Comparable<PinnedDriveReminder> {
	private long station;
	String time;
	boolean arriving;

	private PinnedDriveReminder(long station, String time, boolean arriving) {
		this.station = station;
		this.time = time;
		this.arriving = arriving;
	}

	static List<PinnedDriveReminder> filter(PinnedDrive[] pinnedDrives, Schedule schedule) {
		List<PinnedDriveReminder> list = new ArrayList<>();

		for (PinnedDrive pinnedDrive : pinnedDrives) {

			if (schedule.date.equals(pinnedDrive.schedule.date)) {

				if (schedule.toId == pinnedDrive.schedule.fromId) {
					list.add(new PinnedDriveReminder(pinnedDrive.schedule.fromId, pinnedDrive.getDrive().get(FIELD_DEPARTURE), false));
				}
				if (schedule.fromId == pinnedDrive.schedule.toId) {
					list.add(new PinnedDriveReminder(pinnedDrive.schedule.toId, pinnedDrive.getDrive().get(FIELD_ARRIVAL), true));
				}
			}
		}
		Collections.sort(list);
		return list;
	}

	public String getStationName(Context context) {
		return Stations.getInstance(context).get(station).getLabel();
	}

	public String getTime() {
		return time;
	}

	public boolean isArriving() {
		return arriving;
	}

	@Override
	public int compareTo(@NonNull PinnedDriveReminder o) {
		return time.compareTo(o.time);
	}
}
