package si.erzen.aljaz.vozniredi.activities.pinned;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import si.erzen.aljaz.vozniredi.R;
import si.erzen.aljaz.vozniredi.activities.front.StationSearchFragment;
import si.erzen.aljaz.vozniredi.activities.schedule.ScheduleActivity;
import si.erzen.aljaz.vozniredi.backend.Schedule;
import si.erzen.aljaz.vozniredi.backend.Stations;

import static android.app.Activity.RESULT_OK;
import static si.erzen.aljaz.vozniredi.activities.front.StationSearchFragment.REQUEST_CODE_FROM;
import static si.erzen.aljaz.vozniredi.activities.front.StationSearchFragment.REQUEST_CODE_TO;
import static si.erzen.aljaz.vozniredi.util.Constants.ARG_DATE;
import static si.erzen.aljaz.vozniredi.util.Constants.ARG_FROM_ID;
import static si.erzen.aljaz.vozniredi.util.Constants.ARG_TO_ID;

public class FindConnectionDialog extends DialogFragment implements View.OnClickListener {
	View rootView;

	public static FindConnectionDialog getInstance(Schedule schedule) {
		FindConnectionDialog dialog = new FindConnectionDialog();
		Bundle args = new Bundle();
		args.putString(ARG_DATE, schedule.date);
		args.putLong(ARG_FROM_ID, schedule.fromId);
		args.putLong(ARG_TO_ID, schedule.toId);
		dialog.setArguments(args);
		return dialog;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		setStyle(DialogFragment.STYLE_NO_TITLE, R.style.AppTheme);

		rootView = inflater.inflate(R.layout.dialog_find_connection, container, false);

		rootView.findViewById(R.id.button_add_arrival).setOnClickListener(this);
		rootView.findViewById(R.id.button_add_departure).setOnClickListener(this);

		Bundle args = getArguments();
		Stations stations = Stations.getInstance(getContext());
		((TextView) rootView.findViewById(R.id.from)).setText(stations.get(args.getLong(ARG_FROM_ID)).getLabel());
		((TextView) rootView.findViewById(R.id.to)).setText(stations.get(args.getLong(ARG_TO_ID)).getLabel());

		return rootView;
	}

	@Override
	public void onClick(View view) {
		if (view.getId() == R.id.button_add_arrival || view.getId() == R.id.button_add_departure) {

			int requestCode = view.getId() == R.id.button_add_arrival ? REQUEST_CODE_TO : REQUEST_CODE_FROM;

			StationSearchFragment fragment = StationSearchFragment.newInstance("");
			fragment.setTargetFragment(this, requestCode);
			fragment.show(getFragmentManager());
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_TO || requestCode == REQUEST_CODE_FROM) {
			if (resultCode == RESULT_OK) {
				long stationId = data.getLongExtra(StationSearchFragment.ARG_STATION_ID, 0);

				boolean findConnectionToHere = requestCode == REQUEST_CODE_FROM;

				Bundle args = getArguments();
				Intent intent = new Intent(getContext(), ScheduleActivity.class);
				intent.putExtra(ARG_DATE, args.getString(ARG_DATE));
				intent.putExtra(ARG_FROM_ID, findConnectionToHere ? stationId : args.getLong(ARG_TO_ID));
				intent.putExtra(ARG_TO_ID, findConnectionToHere ? args.getLong(ARG_FROM_ID) : stationId);
				startActivity(intent);
				dismiss();
			}
		}
	}
}
