package si.erzen.aljaz.vozniredi.activities.front;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.vending.billing.IInAppBillingService;

import si.erzen.aljaz.vozniredi.R;
import si.erzen.aljaz.vozniredi.activities.pinned.PinnedDrivesFragment;
import si.erzen.aljaz.vozniredi.activities.settings.SettingsActivity;
import si.erzen.aljaz.vozniredi.util.BillingHelper;

import static si.erzen.aljaz.vozniredi.activities.front.FrontActivity.SectionsPagerAdapter.PINNED_POSITION;
import static si.erzen.aljaz.vozniredi.util.Constants.ARG_PINNED_ID;

public class FrontActivity extends AppCompatActivity {

	IInAppBillingService mService;

	ServiceConnection mServiceConn = new ServiceConnection() {
		@Override
		public void onServiceDisconnected(ComponentName name) {
			mService = null;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mService = IInAppBillingService.Stub.asInterface(service);
			if (BillingHelper.hasFullUpgrade(mService, FrontActivity.this)) {
				findViewById(R.id.buy_message_container).setVisibility(View.GONE);
			}
		}
	};

	SectionsPagerAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_front);
		setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

		ViewPager mViewPager = (ViewPager) findViewById(R.id.container);
		adapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
		mViewPager.setAdapter(adapter);

		TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
		tabLayout.setupWithViewPager(mViewPager);

		// Setup tab icons
		TabLayout.Tab tab = tabLayout.getTabAt(0);
		if (tab != null)
			tab.setIcon(R.drawable.ic_directions_bus_white_24dp);
		tab = tabLayout.getTabAt(1);
		if (tab != null)
			tab.setIcon(R.drawable.ic_pin_white_24dp);

//		Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
//		serviceIntent.setPackage("com.android.vending");
//		bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mService != null) {
			unbindService(mServiceConn);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_front, menu);
		return true;
	}

	public void buyFullVersion(View view) {
		BillingHelper.buyFullUpgrade(mService, this);
	}

	/*@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_BUY_FULL) {
			if (BillingHelper.hasFullUpgrade(mService, this)) {
				findViewById(R.id.buy_message_container).setVisibility(View.INVISIBLE);
			}
		}
	}*/

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.action_settings) {
			startActivity(new Intent(this, SettingsActivity.class));
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		if (intent.hasExtra(ARG_PINNED_ID)) {
			ViewPager viewPager = ((ViewPager) findViewById(R.id.container));
			viewPager.setCurrentItem(PINNED_POSITION);

			if (adapter.pinnedDrivesFragment != null)
				adapter.pinnedDrivesFragment.scrollToId(intent.getLongExtra(ARG_PINNED_ID, 0));
			else
				adapter.scrollToDriveId = intent.getLongExtra(ARG_PINNED_ID, 0);
		}
	}

	public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

		static final int PINNED_POSITION = 1;

		Context context;
		PinnedDrivesFragment pinnedDrivesFragment;
		long scrollToDriveId = -1;

		SectionsPagerAdapter(Context context, FragmentManager fm) {
			super(fm);
			this.context = context;
		}

		@Override
		public Fragment getItem(int position) {
			if (position == 0)
				return new RouteListFragment();
			if (position == PINNED_POSITION) {
				pinnedDrivesFragment = new PinnedDrivesFragment();
				pinnedDrivesFragment.setScrollToDriveId(scrollToDriveId);
				return pinnedDrivesFragment;
			}
			return null;
		}

		@Override
		public int getCount() {
			return 2;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return "";
		}
	}
}
