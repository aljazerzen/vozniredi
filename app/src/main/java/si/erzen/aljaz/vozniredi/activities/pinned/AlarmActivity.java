package si.erzen.aljaz.vozniredi.activities.pinned;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import si.erzen.aljaz.vozniredi.R;
import si.erzen.aljaz.vozniredi.backend.Database;
import si.erzen.aljaz.vozniredi.backend.PinnedDrive;
import si.erzen.aljaz.vozniredi.backend.Stations;
import si.erzen.aljaz.vozniredi.util.Formatter;

import static android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
import static android.view.WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED;
import static android.view.WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON;
import static si.erzen.aljaz.vozniredi.util.Constants.ARG_PINNED_ID;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_ARRIVAL;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_DEPARTURE;
import static si.erzen.aljaz.vozniredi.util.Constants.PREF_SNOOZE_DURATION;

public class AlarmActivity extends AppCompatActivity {

	PinnedDrive drive;
	private MediaPlayer mMediaPlayer = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(FLAG_TURN_SCREEN_ON | FLAG_SHOW_WHEN_LOCKED | FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.activity_alarm);

		if (!getIntent().hasExtra(ARG_PINNED_ID)) {
			finish();
			return;
		}
		drive = Database.getInstance(this).getPinnedDrive(getIntent().getLongExtra(ARG_PINNED_ID, 0));
		if (drive.getAlarmTimestamp() == -1) {
			finish();
			return;
		}

		// General data about the drive
		long departure = Formatter.fromScheduleTime(drive.getDrive().get(FIELD_DEPARTURE)).getTime();
		departure += Formatter.fromISODate(drive.getSchedule().date).getTime();
		String alarmTime = Formatter.diffToHumanElapsed(System.currentTimeMillis(), departure, this);
		alarmTime += " (" + drive.getDrive().get(FIELD_DEPARTURE) + ")";
		getView(R.id.alarm_time).setText(alarmTime);

		getView(R.id.from).setText(Stations.getInstance(this).get(drive.getSchedule().fromId).getLabel());
		getView(R.id.to).setText(Stations.getInstance(this).get(drive.getSchedule().toId).getLabel());
		getView(R.id.arrival).setText(drive.getDrive().get(FIELD_ARRIVAL));
//		getView(R.id.price).setText(getString(R.string.price, drive.getDrive().get(FIELD_PRICE)));
//		getView(R.id.distance).setText(getString(R.string.distance, drive.getDrive().get(FIELD_DISTANCE)));
		getView(R.id.duration).setText(Formatter.ISOTimeToHumanElapsed(drive.getDrive().get(FIELD_DEPARTURE), drive.getDrive().get(FIELD_ARRIVAL), this));

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				if (mMediaPlayer == null) {
					try {
						Uri alert = RingtoneManager.getActualDefaultRingtoneUri(AlarmActivity.this, RingtoneManager.TYPE_ALARM);
						if (alert == null)
							alert = RingtoneManager.getActualDefaultRingtoneUri(AlarmActivity.this, RingtoneManager.TYPE_ALL);
						if (alert != null) {
							mMediaPlayer = new MediaPlayer();
							mMediaPlayer.setDataSource(AlarmActivity.this, alert);

							final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

							if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
								mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
								mMediaPlayer.setLooping(true);
								mMediaPlayer.prepare();
								mMediaPlayer.start();
							}
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}, 500);
	}

	private TextView getView(int id) {
		return (TextView) findViewById(id);
	}

	private void stopAlarm() {
		if (mMediaPlayer != null) {
			mMediaPlayer.stop();
			mMediaPlayer.reset();
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
	}

	public void dismiss(View view) {
		stopAlarm();
		finish();
	}

	@Override
	protected void onStop() {
		super.onStop();
		stopAlarm();
	}

	public void snooze(View view) {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		int snoozeDurationMin = Integer.parseInt(sharedPref.getString(PREF_SNOOZE_DURATION, "5"));

		drive.setAlarm(this, System.currentTimeMillis() + snoozeDurationMin * 60 * 1000);
		Database.getInstance(this).save(drive);
		Toast.makeText(this, getResources().getQuantityString(R.plurals.snoozed, snoozeDurationMin, snoozeDurationMin), Toast.LENGTH_LONG).show();
		dismiss(null);
	}
}
