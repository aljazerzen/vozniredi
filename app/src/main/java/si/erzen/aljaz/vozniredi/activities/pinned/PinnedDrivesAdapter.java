package si.erzen.aljaz.vozniredi.activities.pinned;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;

import si.erzen.aljaz.vozniredi.R;
import si.erzen.aljaz.vozniredi.backend.PinnedDrive;
import si.erzen.aljaz.vozniredi.backend.PinnedDriveCombined;
import si.erzen.aljaz.vozniredi.backend.Stations;
import si.erzen.aljaz.vozniredi.util.Formatter;

import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_ARRIVAL;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_DEPARTURE;

class PinnedDrivesAdapter extends RecyclerView.Adapter<PinnedDrivesAdapter.ViewHolder> {
	private List<PinnedDriveCombined> pinnedDrives;
	private OnPinnedDriveActionListener listener;

	PinnedDrivesAdapter(PinnedDrive[] drives, OnPinnedDriveActionListener listener) {
		replaceDataSet(drives);
		this.listener = listener;
	}

	void replaceDataSet(PinnedDrive[] drives) {
		pinnedDrives = PinnedDriveCombined.fromArray(drives);
		notifyDataSetChanged();
	}

	void mergeDataSet(PinnedDrive[] drives) {
		List<PinnedDriveCombined> newList = PinnedDriveCombined.fromArray(drives);
		for (int i = 0; i < newList.size(); i++) {
			if (i >= pinnedDrives.size() || !newList.get(i).equals(pinnedDrives.get(i))) {
				pinnedDrives.add(i, newList.get(i));
				notifyItemInserted(i);
			}
		}
	}

	void updateItem(PinnedDrive drive) {
		for (int i = 0; i < pinnedDrives.size(); i++) {
			for (int j = 0; j < pinnedDrives.get(i).size(); j++) {
				if (pinnedDrives.get(i).get(j).equals(drive)) {
					pinnedDrives.get(i).set(j, drive);
					notifyItemChanged(i);
				}
			}
		}
	}

	void removeFromDataSet(PinnedDriveCombined drive) {
		for (int i = 0; i < pinnedDrives.size(); i++) {
			if (drive.equals(pinnedDrives.get(i))) {
				pinnedDrives.remove(i);
				notifyItemRemoved(i);
			}
		}
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		return new ViewHolder(inflater.inflate(R.layout.listitem_pinned_drive, parent, false));
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, int position) {
		PinnedDriveCombined drive = pinnedDrives.get(position);
		Context context = holder.itemView.getContext();

		// General data about the drive
		holder.getTextView(R.id.date).setText(drive.first().getSchedule().getDateTitle(context));

		holder.getTextView(R.id.route).setText(drive.getRoute(context));
		holder.getTextView(R.id.departure).setText(drive.departure());
		holder.getTextView(R.id.arrival).setText(drive.arrival());
		holder.getTextView(R.id.duration).setText(Formatter.ISOTimeToHumanElapsed(drive.departure(), drive.arrival(), context));

		holder.adapter.setCombined(drive);

		// Alarm related stuff
		boolean alarmSet = drive.first().hasAlarmSet();
		holder.itemView.findViewById(R.id.alarm_container).setVisibility(alarmSet ? View.VISIBLE : View.GONE);
		if (alarmSet) {
			String alarmTime = Formatter.diffToHumanElapsed(Calendar.getInstance().getTimeInMillis(), drive.first().getAlarmTimestamp(), context);
			String alarmTimeDiff = Formatter.diffToHumanElapsed(drive.first().getAlarmTimestamp(), drive.first().getDepartureDatetime(), context);
			holder.getTextView(R.id.alarm_time).setText(context.getString(R.string.reminder_in, alarmTime, alarmTimeDiff));
		}
		holder.itemView.findViewById(R.id.button_alarm_add).setVisibility(alarmSet ? View.GONE : View.VISIBLE);
		holder.itemView.findViewById(R.id.button_alarm_remove).setVisibility(alarmSet ? View.VISIBLE : View.GONE);
	}

	@Override
	public int getItemCount() {
		return pinnedDrives.size();
	}

	int getIndex(long id) {
		for (int i = 0; i < pinnedDrives.size(); i++) {
			for (int j = 0; j < pinnedDrives.get(i).size(); j++) {
				if (pinnedDrives.get(i).get(j).getId() == id)
					return i;
			}
		}
		return 0;
	}

	interface OnPinnedDriveActionListener {
		void onShare(PinnedDriveCombined pinnedDrive);

		void onFindConnection(PinnedDriveCombined pinnedDrive);

		void onAlarmAdd(PinnedDrive pinnedDrive);

		void onAlarmRemove(PinnedDrive pinnedDrive);

		void onDelete(PinnedDriveCombined pinnedDrive);
	}

	class ViewHolder extends RecyclerView.ViewHolder {
		IntermediateStationAdapter adapter;

		ViewHolder(View itemView) {
			super(itemView);
			// on click listeners for buttons
			itemView.findViewById(R.id.button_share).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					listener.onShare(pinnedDrives.get(getAdapterPosition()));
				}
			});
			itemView.findViewById(R.id.button_find_connection).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					listener.onFindConnection(pinnedDrives.get(getAdapterPosition()));
				}
			});
			itemView.findViewById(R.id.button_alarm_add).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					listener.onAlarmAdd(pinnedDrives.get(getAdapterPosition()).first());
				}
			});
			itemView.findViewById(R.id.button_alarm_remove).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					listener.onAlarmRemove(pinnedDrives.get(getAdapterPosition()).first());
					notifyItemChanged(getAdapterPosition());
				}
			});
			itemView.findViewById(R.id.button_delete).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					listener.onDelete(pinnedDrives.get(getAdapterPosition()));
				}
			});
			adapter = new IntermediateStationAdapter();
			((RecyclerView) itemView.findViewById(R.id.intermediate_station_recycler)).setAdapter(adapter);
		}

		TextView getTextView(int id) {
			return (TextView) itemView.findViewById(id);
		}
	}

	class IntermediateStationAdapter extends RecyclerView.Adapter<IntermediateStationAdapter.ViewHolder> {
		PinnedDriveCombined combined;

		public void setCombined(PinnedDriveCombined combined) {
			this.combined = combined;
			notifyDataSetChanged();
		}

		@Override
		public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_intermediate_station, parent, false);
			return new ViewHolder(view);
		}

		@Override
		public void onBindViewHolder(ViewHolder holder, int position) {
			Context context = holder.itemView.getContext();

			boolean last = position == combined.size();

			holder.itemView.findViewById(R.id.station_marker_line_up).setVisibility(position == 0 ? View.INVISIBLE : View.VISIBLE);
			holder.itemView.findViewById(R.id.station_marker_line_down).setVisibility(last ? View.INVISIBLE : View.VISIBLE);

			long stationId = last ? combined.get(position - 1).getSchedule().toId : combined.get(position).getSchedule().fromId;
			String station = Stations.getInstance(context).get(stationId).getLabel();
			holder.getTextView(R.id.station_name).setText(station);

			holder.itemView.findViewById(R.id.arrival).setVisibility(position == 0 ? View.GONE : View.VISIBLE);
			holder.itemView.findViewById(R.id.departure).setVisibility(last ? View.GONE : View.VISIBLE);

			String departure = null, arrival = null;

			if (position != 0) {
				arrival = combined.get(position - 1).getDrive().get(FIELD_ARRIVAL);
				((TextView) holder.itemView.findViewById(R.id.arrival)).setText(arrival);
			}

			if (!last) {
				departure = combined.get(position).getDrive().get(FIELD_DEPARTURE);
				((TextView) holder.itemView.findViewById(R.id.departure)).setText(departure);
			}

			if (position != 0 && !last) {
				String duration = Formatter.ISOTimeToHumanElapsed(arrival, departure, context);
				holder.getTextView(R.id.waiting_time).setText(context.getString(R.string.waiting_time, duration));
			} else {
				holder.getTextView(R.id.waiting_time).setText(null);
			}
		}

		@Override
		public int getItemCount() {
			return combined == null || combined.size() <= 1 ? 0 : combined.size() + 1;
		}

		class ViewHolder extends RecyclerView.ViewHolder {
			ViewHolder(View itemView) {
				super(itemView);
			}

			TextView getTextView(int id) {
				return (TextView) itemView.findViewById(id);
			}
		}
	}
}
