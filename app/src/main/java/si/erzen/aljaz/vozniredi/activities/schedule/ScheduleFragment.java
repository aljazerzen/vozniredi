package si.erzen.aljaz.vozniredi.activities.schedule;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.VolleyError;

import java.util.List;

import si.erzen.aljaz.vozniredi.R;
import si.erzen.aljaz.vozniredi.activities.front.FrontActivity;
import si.erzen.aljaz.vozniredi.backend.Database;
import si.erzen.aljaz.vozniredi.backend.Drive;
import si.erzen.aljaz.vozniredi.backend.Fetcher;
import si.erzen.aljaz.vozniredi.backend.PinnedDrive;
import si.erzen.aljaz.vozniredi.backend.Schedule;
import si.erzen.aljaz.vozniredi.backend.ScheduleDataSetItem;
import si.erzen.aljaz.vozniredi.backend.ScheduleQuery;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static si.erzen.aljaz.vozniredi.util.Constants.ARG_DATE;
import static si.erzen.aljaz.vozniredi.util.Constants.ARG_FROM_ID;
import static si.erzen.aljaz.vozniredi.util.Constants.ARG_PINNED_ID;
import static si.erzen.aljaz.vozniredi.util.Constants.ARG_TO_ID;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_ARRIVAL;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_DEPARTURE;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_DURATION;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_OPERATOR;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_REGIME;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_REGIME_LONG;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_TTD;
import static si.erzen.aljaz.vozniredi.util.Constants.PREF_AUTO_DOWNLOAD;

public class ScheduleFragment extends Fragment implements Fetcher.ScheduleListener, ScheduleAdapter.OnDriveActionListener, View.OnClickListener {

	public ScheduleAdapter scheduleAdapter;

	Schedule schedule;
	View rootView, progressBar, downloadContainer;
	RecyclerView recyclerView;

	public ScheduleFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_schedule_day, container, false);

		recyclerView = (RecyclerView) rootView.findViewById(R.id.list);
		progressBar = rootView.findViewById(R.id.progressBar);
		downloadContainer = rootView.findViewById(R.id.download_container);

		// Load data
		Bundle args = getArguments();
		if (args != null) {
			final long fromId = args.getLong(ARG_FROM_ID, -1);
			final long toId = args.getLong(ARG_TO_ID, -1);

			if (fromId >= 0 && toId >= 0) {

				final String date = args.containsKey(ARG_DATE) ? args.getString(ARG_DATE) : null;
				schedule = new Schedule(fromId, toId, date);

				if (!schedule.isGeneral())
					scheduleAdapter = new ScheduleAdapter(new String[]{
							FIELD_DEPARTURE,
							FIELD_ARRIVAL,
							FIELD_TTD,
							FIELD_DURATION,
							FIELD_OPERATOR
					}, this, true);
				else
					scheduleAdapter = new ScheduleAdapter(new String[]{
							FIELD_DEPARTURE,
							FIELD_ARRIVAL,
							FIELD_REGIME,
							FIELD_DURATION,
							FIELD_OPERATOR,
							FIELD_TTD,
							FIELD_REGIME_LONG
					}, this, false);
				recyclerView.setAdapter(scheduleAdapter);

				// Load from database
				ScheduleQuery query = date == null ? new ScheduleQuery(fromId, toId, true) : new ScheduleQuery(fromId, toId, date);
				Schedule savedSchedule = Database.getInstance(getContext()).getSchedule(query);
				if (savedSchedule != null) {
					onResponse(savedSchedule);
					// TODO check if schedule should be updated
				} else {
					SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
					if (sharedPref.getBoolean(PREF_AUTO_DOWNLOAD, true)) {
						fetchFromInternet();
					} else {
						downloadContainer.setVisibility(VISIBLE);
					}
				}
			}
		}

		rootView.findViewById(R.id.button_download).setOnClickListener(this);
		return rootView;
	}

	@Override
	public void onClick(View view) {
		fetchFromInternet();
	}

	private void fetchFromInternet() {
		progressBar.setVisibility(VISIBLE);
		// Fetch from internet
		Fetcher.getInstance(getContext()).fetchSchedule(schedule, this);
	}

	@Override
	public void onErrorResponse(VolleyError error) {
		downloadContainer.setVisibility(VISIBLE);
		progressBar.setVisibility(GONE);
		Log.e("vozniredi", error.toString());
		if (!isDetached() && getContext() != null)
			Toast.makeText(getContext(), R.string.network_error, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onResponse(@NonNull final Schedule response) {
		if (isDetached() || getContext() == null)
			return;

		schedule = response;

		final List<ScheduleDataSetItem> dataSet;
		if (!schedule.isGeneral()) {
			PinnedDrive[] drives = Database.getInstance(getContext()).getPinnedDrives();
			dataSet = schedule.composeDataSetWithPinnedReminders(getContext(), drives);
		} else {
			dataSet = schedule.getDataSet(getContext());
		}

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				rootView.findViewById(R.id.download_container).setVisibility(GONE);
				progressBar.setVisibility(GONE);

				// Recycler fade-down in animation
				recyclerView.setTranslationY(100);
				recyclerView.setAlpha(0);
				recyclerView.animate().alpha(1).translationY(0).start();

				scheduleAdapter.updateDataSet(dataSet);

				rootView.findViewById(R.id.no_bus).setVisibility(dataSet.isEmpty() ? VISIBLE : GONE);
			}
		}, 500);
	}

	@Override
	public void onPin(Drive drive) {
		PinnedDrive pinnedDrive = new PinnedDrive(schedule, drive);
		Database.getInstance(getContext()).save(pinnedDrive);
		final long id = pinnedDrive.getId();
		Snackbar.make(rootView, R.string.snackbar_drive_pinned, Snackbar.LENGTH_LONG).setAction(R.string.action_show, new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(getContext(), FrontActivity.class);
				intent.setFlags(FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(ARG_PINNED_ID, id);
				startActivity(intent);
			}
		}).show();
	}
}