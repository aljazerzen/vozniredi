package si.erzen.aljaz.vozniredi.backend;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_ARRIVAL;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_DEPARTURE;

public class PinnedDriveCombined extends ArrayList<PinnedDrive> {

	private Schedule schedule;

	public static List<PinnedDriveCombined> fromArray(PinnedDrive[] drives) {
		List<PinnedDriveCombined> r = new ArrayList<>();

		for (int i = 0; i < drives.length; i++) {
			if (i == 0 ||
					!drives[i - 1].getSchedule().date.equals(drives[i].getSchedule().date) ||
					drives[i - 1].getSchedule().toId != drives[i].getSchedule().fromId) {
				r.add(new PinnedDriveCombined());
			}
			r.get(r.size() - 1).add(drives[i]);
		}
		return r;
	}

	public PinnedDrive first() {
		return get(0);
	}

	private PinnedDrive last() {
		return get(size() - 1);
	}

	// Returns combined schedule for all drives aggregated. Cached
	public Schedule getSchedule() {
		if (schedule == null)
			schedule = new Schedule(first().getSchedule().fromId, last().getSchedule().toId, first().getSchedule().date);
		return schedule;
	}

	public String departure() {
		return first().getDrive().get(FIELD_DEPARTURE);
	}

	public String arrival() {
		return last().getDrive().get(FIELD_ARRIVAL);
	}

	/*
	public String price() {
		if (size() == 1)
			return first().getDrive().get(FIELD_PRICE);

		double total = 0;
		for (PinnedDrive d : this) {
			total += Double.parseDouble(d.getDrive().get(FIELD_PRICE));
		}
		return String.format(Locale.ENGLISH, "%.2f", total);
	}

	public String distance() {
		if (size() == 1)
			return first().getDrive().get(FIELD_DISTANCE);

		int total = 0;
		for (PinnedDrive d : this) {
			total += Integer.parseInt(d.getDrive().get(FIELD_DISTANCE));
		}
		return Integer.toString(total);
	}
	*/

	public String getRoute(Context context) {
		return getSchedule().getRoute(context);
	}

	public boolean equals(PinnedDriveCombined o) {
		if (o.size() != size())
			return false;
		for (int i = 0; i < size(); i++) {
			if (!o.get(i).equals(get(i)))
				return false;
		}
		return true;
	}
}
