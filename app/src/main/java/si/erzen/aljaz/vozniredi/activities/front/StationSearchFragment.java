package si.erzen.aljaz.vozniredi.activities.front;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import si.erzen.aljaz.vozniredi.R;
import si.erzen.aljaz.vozniredi.backend.Stations;

import static android.app.Activity.RESULT_OK;
import static si.erzen.aljaz.vozniredi.util.Constants.MAX_RECENT_STATIONS;
import static si.erzen.aljaz.vozniredi.util.Constants.PREF_RECENT;
import static si.erzen.aljaz.vozniredi.util.Constants.PREF_RECENT_STATIONS;

public class StationSearchFragment extends DialogFragment {
	private static final String ARG_TEXT = "text";
	public static final String ARG_STATION = "station";
	public static final String ARG_STATION_ID = "station_id";
	public static final int REQUEST_CODE_FROM = 0x124;
	public static final int REQUEST_CODE_TO = 0x287;

	private static final String FRAGMENT_TAG = "stationSearchFragment";

	private String text;
	SuggestionAdapter adapter;

	Map<Long, Integer> reverseRecentMap;

	public static StationSearchFragment newInstance(String text) {
		StationSearchFragment fragment = new StationSearchFragment();
		Bundle args = new Bundle();
		args.putString(ARG_TEXT, text);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			text = getArguments().getString(ARG_TEXT);
		}
		SharedPreferences settings = getContext().getSharedPreferences(PREF_RECENT, 0);
		String recent = settings.getString(PREF_RECENT_STATIONS, "[]");

		reverseRecentMap = new TreeMap<>();

		try {
			JSONArray jsonArray = new JSONArray(recent);
			for (int i = 0; i < jsonArray.length(); i++) {
				reverseRecentMap.put(jsonArray.getLong(i), i);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		adapter = new SuggestionAdapter();
	}

	public void show(FragmentManager fragmentManager) {
		StationSearchFragment fragment = (StationSearchFragment) fragmentManager.findFragmentByTag(FRAGMENT_TAG);
		if (fragment != null)
			fragment.dismiss();

		super.show(fragmentManager, FRAGMENT_TAG);
	}

	public void dismiss() {
		// close keyboard
		View view = getView();
		if (view != null) {
			InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}

		super.dismiss();
	}

	private void addRecentStation(long stationId) {

		SharedPreferences settings = getContext().getSharedPreferences(PREF_RECENT, 0);
		SharedPreferences.Editor editor = settings.edit();

		JSONArray jsonArray = new JSONArray();

		// Add new station
		jsonArray.put(stationId);

		// Find old stations in reverseRecentList
		for (int i = 0; i < MAX_RECENT_STATIONS - 1; i++) {
			stationId = -1;

			for (Map.Entry<Long, Integer> e : reverseRecentMap.entrySet()) {
				if (e.getValue().equals(i)) {
					stationId = e.getKey();
					break;
				}
			}
			if (stationId != -1) {
				jsonArray.put(stationId);
			}
		}

		editor.putString(PREF_RECENT_STATIONS, jsonArray.toString());
		editor.apply();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

		final View rootView = inflater.inflate(R.layout.fragment_station_search, container, false);
		final RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.suggestion_recycler);
		recyclerView.setAdapter(adapter);
		recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

		rootView.findViewById(R.id.button_close).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				dismiss();
			}
		});
		final EditText queryInput = (EditText) rootView.findViewById(R.id.query_input);
		queryInput.setText(text);
		adapter.filter(text);

		final Handler handler = new Handler();

		queryInput.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void onTextChanged(final CharSequence charSequence, int i, int i1, int i2) {
				handler.removeCallbacksAndMessages(null);
				handler.postDelayed(new Runnable() {
					@Override
					public void run() {
						adapter.filter(charSequence.toString());
					}
				}, 100);

				recyclerView.scrollToPosition(0);
			}

			@Override
			public void afterTextChanged(Editable editable) {

			}
		});
		queryInput.requestFocus();

		// show keyboard
		/*
		rootView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
			@Override
			public boolean onPreDraw() {
				rootView.getViewTreeObserver().removeOnPreDrawListener(this);
				InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
				return true;
			}
		});
		*/

		// Erase button
		rootView.findViewById(R.id.button_erase).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				queryInput.setText("");
			}
		});
		return rootView;
	}

	private class SuggestionAdapter extends RecyclerView.Adapter<SuggestionAdapter.ViewHolder> {

		private class StationWrapper implements Comparable<StationWrapper> {
			Stations.Station station;
			int recentIndex;

			StationWrapper(Stations.Station s) {
				station = s;
				Integer index = reverseRecentMap.get(s.getId());
				recentIndex = index != null ? index : -1;
			}

			public boolean equals(StationWrapper wrapper) {
				return station.getId() == wrapper.station.getId();
			}

			@Override
			public int compareTo(@NonNull StationWrapper o) {
				if (recentIndex >= 0 && o.recentIndex < 0)
					return -1;
				if (recentIndex < 0 && o.recentIndex >= 0)
					return 1;
				if (recentIndex >= 0 && o.recentIndex >= 0)
					return recentIndex - o.recentIndex;
				// neither of the stations is in the recent list
				return station.getLabel().compareTo(o.station.getLabel());
			}
		}

		List<StationWrapper> dataSet;
		List<StationWrapper> filteredDataSet;

		SuggestionAdapter() {
			dataSet = new ArrayList<>();
			for (Stations.Station s : Stations.getInstance(getContext()).values()) {
				dataSet.add(new StationWrapper(s));
			}
			Collections.sort(dataSet);

			filteredDataSet = new ArrayList<>(dataSet.size());
			filteredDataSet.addAll(dataSet);
		}

		void filter(String searchQuery) {
//			List<StationWrapper> oldFilteredDataSet = filteredDataSet;
			filteredDataSet = new ArrayList<>();
			boolean added[] = new boolean[dataSet.size()];

			searchQuery = searchQuery.toLowerCase();

			for (int i = 0; i < dataSet.size(); i++) {
				StationWrapper e = dataSet.get(i);
				if (e.station.getLabel().toLowerCase().contains(searchQuery)) {
					filteredDataSet.add(e);
					added[i] = true;
				}
			}

			for (int i = 0; i < dataSet.size(); i++) {
				StationWrapper e = dataSet.get(i);
				if (!added[i] || e.station.getNaselje().toLowerCase().contains(searchQuery)) {
					filteredDataSet.add(e);
					added[i] = true;
				}
			}

			for (int i = 0; i < dataSet.size(); i++) {
				StationWrapper e = dataSet.get(i);
				if (!added[i] || e.station.getObcina().toLowerCase().contains(searchQuery)) {
					filteredDataSet.add(e);
					added[i] = true;
				}
			}
			notifyDataSetChanged();

			/*
			// Notify adapter about changes
			int i = 0, j = 0;
			for (; i < filteredDataSet.size() && j < oldFilteredDataSet.size(); ) {
				if (filteredDataSet.get(i).equals(oldFilteredDataSet.get(j))) {
					i++;
					j++;
				} else {
					if (filteredDataSet.get(i).compareTo(oldFilteredDataSet.get(j)) > 0) {
						notifyItemRemoved(i);
						j++;
					} else {
						notifyItemInserted(i);
						i++;
					}
				}
			}
			if (i < filteredDataSet.size()) {
				notifyItemRangeInserted(i, filteredDataSet.size() - i);
			}
			if (j < oldFilteredDataSet.size()) {
				notifyItemRangeRemoved(i, oldFilteredDataSet.size() - j);
			}*/
		}

		class ViewHolder extends RecyclerView.ViewHolder {
			ViewHolder(View itemView) {
				super(itemView);
				itemView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						Intent intent = new Intent();
						intent.putExtra(ARG_STATION, filteredDataSet.get(getAdapterPosition()).station.getLabel());
						long stationId = filteredDataSet.get(getAdapterPosition()).station.getId();
						intent.putExtra(ARG_STATION_ID, stationId);
						getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent);
						addRecentStation(stationId);
						dismiss();
					}
				});
			}

			View findViewById(int id) {
				return itemView.findViewById(id);
			}
		}

		@Override
		public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			View view = LayoutInflater.from(getContext()).inflate(R.layout.listitem_station_search, parent, false);
			return new ViewHolder(view);
		}

		@Override
		public void onBindViewHolder(final ViewHolder holder, int position) {
			((TextView) holder.findViewById(android.R.id.text1)).setText(filteredDataSet.get(position).station.getLabel());
			holder.findViewById(R.id.list_icon).setVisibility(filteredDataSet.get(position).recentIndex < 0 ? View.INVISIBLE : View.VISIBLE);
		}

		@Override
		public int getItemCount() {
			return filteredDataSet.size();
		}
	}
}
