package si.erzen.aljaz.vozniredi.activities.front;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import si.erzen.aljaz.vozniredi.R;
import si.erzen.aljaz.vozniredi.backend.Schedule;

class RouteListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

	private ArrayList<Schedule> scheduleList;
	private OnRouteClickListener mListener;

	RouteListAdapter(Schedule[] items, OnRouteClickListener listener) {
		scheduleList = new ArrayList<>();
		Collections.addAll(scheduleList, items);
		mListener = listener;
	}

	void updateDataSet(Schedule[] items) {
		scheduleList = new ArrayList<>();
		Collections.addAll(scheduleList, items);
		notifyDataSetChanged();
	}

	void deleteItem(int position) {
		scheduleList.remove(position);
		notifyItemRemoved(position);
	}

	public void addItem(Schedule schedule, int position) {
		scheduleList.add(position, schedule);
		notifyItemInserted(position);
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		return new ViewHolder(inflater.inflate(R.layout.listitem_route, parent, false));
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		((ViewHolder) holder).bind(scheduleList.get(position));
	}

	@Override
	public int getItemCount() {
		return scheduleList.size();
	}

	private class ViewHolder extends RecyclerView.ViewHolder {
		final TextView mRouteView;
		Schedule schedule;

		ViewHolder(View view) {
			super(view);
			mRouteView = (TextView) view.findViewById(R.id.route);
			view.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (null != mListener) {
						mListener.onRouteInvoke(schedule);
					}
				}
			});
			view.findViewById(R.id.button_delete).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					if (null != mListener) {
						mListener.onRouteDelete(schedule, getAdapterPosition());
					}
				}
			});
		}

		void bind(Schedule schedule) {
			this.schedule = schedule;
			mRouteView.setText(schedule.getRoute(mRouteView.getContext()));
		}
	}
}
