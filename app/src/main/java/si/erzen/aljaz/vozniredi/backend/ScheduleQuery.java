package si.erzen.aljaz.vozniredi.backend;

import java.util.Calendar;

import si.erzen.aljaz.vozniredi.util.Formatter;

public class ScheduleQuery {

	private long fromId = -1;
	private long toId = -1;
	private String date;
	private boolean general;

	private String[] fields = Database.SCHEDULES_FIELDS_ALL;
	private boolean groupByRoute = false;
	private boolean allOnRoute = false;
	private boolean beforeToday = false;

	public ScheduleQuery() {

	}

	public ScheduleQuery(Schedule s) {
		fromId = s.fromId;
		toId = s.toId;
		general = s.isGeneral();
		date = s.date;
	}

	public ScheduleQuery(long fromId, long toId, String date) {
		this.fromId = fromId;
		this.toId = toId;
		this.date = date;
	}

	public ScheduleQuery(long fromId, long toId, boolean general) {
		this.fromId = fromId;
		this.toId = toId;
		this.general = general;
	}

	String[] getWhereQueryData() {
		if (fromId == -1 || toId == -1)
			return null;
		if (beforeToday)
			return new String[]{"" + fromId, "" + toId, Formatter.toISODate(Calendar.getInstance())};
		if (allOnRoute || general || date == null)
			return new String[]{"" + fromId, "" + toId};
		return new String[]{"" + fromId, "" + toId, date};
	}

	String getWhereQuery() {
		if (fromId == -1 || toId == -1)
			return null;
		if (beforeToday)
			return Database.WHERE_ROUTE_ID_DATE_LT;
		if (allOnRoute)
			return Database.WHERE_ROUTE_ALL;
		if (general)
			return Database.WHERE_ROUTE_ID_GENERAL;
		if (date == null)
			return Database.WHERE_ROUTE_ID;
		return Database.WHERE_ROUTE_ID_DATE;
	}

	public String[] getFields() {
		return fields;
	}

	public ScheduleQuery noData() {
		if(!groupByRoute)
			fields = Database.SCHEDULES_FIELDS_NO_DATA;
		return this;
	}

	public ScheduleQuery groupByRoute() {
		groupByRoute = true;
		fields = Database.SCHEDULES_FIELDS_ROUTE;
		return this;
	}

	public String getGroupByFields() {
		return groupByRoute ? Database.GROUP_BY_ROUTE : null;
	}

	public ScheduleQuery allOnRoute() {
		allOnRoute = true;
		return this;
	}

	public ScheduleQuery beforeToday() {
		beforeToday = true;
		return this;
	}
}
