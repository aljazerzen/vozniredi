package si.erzen.aljaz.vozniredi.activities.schedule;

import android.graphics.Path;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.transition.ChangeBounds;
import android.transition.PathMotion;
import android.view.MenuItem;
import android.widget.TextView;

import si.erzen.aljaz.vozniredi.R;
import si.erzen.aljaz.vozniredi.backend.Schedule;
import si.erzen.aljaz.vozniredi.util.Constants;

public class ScheduleActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_schedule_day);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			ChangeBounds changeBounds = new ChangeBounds();
			changeBounds.setPathMotion(new PathMotion() {
				@Override
				public Path getPath(float startX, float startY, float endX, float endY) {
					Path path = new Path();
					path.moveTo(startX, startY);
					path.quadTo(Math.max(startX, endX), Math.max(startY, endY), endX, endY);
					return path;
				}
			});
			getWindow().setSharedElementEnterTransition(changeBounds);
			getWindow().setSharedElementExitTransition(changeBounds);
		}

		long fromId = getIntent().getLongExtra(Constants.ARG_FROM_ID, 0);
		long toId = getIntent().getLongExtra(Constants.ARG_TO_ID, 0);
		String date = getIntent().getStringExtra(Constants.ARG_DATE);
		Schedule schedule = new Schedule(fromId, toId, date);

		ActionBar bar = getSupportActionBar();
		if (bar != null) {
			bar.setHomeButtonEnabled(true);
			bar.setTitle(schedule.getRoute(this));
		}
		((TextView) findViewById(R.id.title_date)).setText(schedule.getDateTitle(this));
		findViewById(R.id.title_date).requestFocus();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			findViewById(R.id.title_date).setTransitionName("date_title_" + date);
		}

		Fragment fragment = new ScheduleFragment();
		fragment.setArguments(getIntent().getExtras());

		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();

		transaction.add(R.id.scroll_container, fragment);
		transaction.commit();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				supportFinishAfterTransition();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
