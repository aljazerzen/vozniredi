package si.erzen.aljaz.vozniredi.activities.route;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import si.erzen.aljaz.vozniredi.R;
import si.erzen.aljaz.vozniredi.activities.schedule.ScheduleActivity;
import si.erzen.aljaz.vozniredi.backend.Database;
import si.erzen.aljaz.vozniredi.backend.Schedule;
import si.erzen.aljaz.vozniredi.backend.ScheduleQuery;
import si.erzen.aljaz.vozniredi.util.Formatter;

import static si.erzen.aljaz.vozniredi.util.Constants.ARG_DATE;
import static si.erzen.aljaz.vozniredi.util.Constants.ARG_FROM_ID;
import static si.erzen.aljaz.vozniredi.util.Constants.ARG_TO_ID;

public class DateSelectorFragment extends Fragment implements BundleAdapter.ItemInteractionListener {

	View rootView;
	MaterialCalendarView calendarView;
	View datePickerContainer;
	View datePickerFAB;
	BundleAdapter<Schedule> dateAdapter;

	private long fromId;
	private long toId;

	public void setRoute(long fromId, long toId) {
		this.toId = toId;
		this.fromId = fromId;
		dateAdapter.updateDataSet(composeDataSet());
	}

	public DateSelectorFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_date_selector, container, false);

		// Load data
		Integer[] titles = new Integer[]{R.string.past_schedules, R.string.upcoming_schedules};
		dateAdapter = new BundleAdapter<>(getContext(), R.layout.listitem_date, titles, new BundleAdapter.Binder<Schedule>() {
			@Override
			public void bind(BundleAdapter.ViewHolder holder, Schedule schedule) {
				((TextView) holder.view.findViewById(R.id.title)).setText(schedule.getDateTitle(getContext()));
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
					holder.view.findViewById(R.id.title).setTransitionName("date_title_" + schedule.date);
				}
				((TextView) holder.view.findViewById(R.id.subtitle)).setText(schedule.getDateSubtitle(getContext()));
				if (schedule.isSaved())
					((TextView) holder.view.findViewById(R.id.subtitle_right)).setText(R.string.saved);
			}
		}, new BundleAdapter.Comparator<Schedule>() {
			@Override
			public boolean isBefore(Schedule a, Schedule b) {
				return a.date.compareTo(b.date) > 0;
			}
		}, this);
		dateAdapter.disallowCollapsing(1);
		dateAdapter.setExtended(0, false);
		dateAdapter.updateDataSet(composeDataSet());

		RecyclerView dateRecycler = (RecyclerView) rootView.findViewById(R.id.dateRecycler);
		dateRecycler.setAdapter(dateAdapter);
		dateRecycler.setLayoutManager(new LinearLayoutManager(getContext()));

		// Recycler view clipping fix
		rootView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
			@Override
			public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
				rootView.findViewById(R.id.dateRecycler).setMinimumHeight(rootView.getHeight() - rootView.findViewById(R.id.date_general).getHeight() - 80);
				rootView.removeOnLayoutChangeListener(this);
			}
		});

		// Init swipe to delete
		new ItemTouchHelper(new DateItemSwipeCallbackHelper(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT)).attachToRecyclerView(dateRecycler);

		// Calendar on select
		calendarView = (MaterialCalendarView) rootView.findViewById(R.id.calendarView);
		calendarView.newState().setMinimumDate(Calendar.getInstance()).commit();
		calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
			@Override
			public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
				Intent intent = new Intent(getContext(), ScheduleActivity.class);
				intent.putExtra(ARG_FROM_ID, fromId);
				intent.putExtra(ARG_TO_ID, toId);
				intent.putExtra(ARG_DATE, String.format(Locale.ENGLISH, "%04d-%02d-%02d", date.getYear(), date.getMonth() + 1, date.getDay()));
				startActivity(intent);
			}
		});

		datePickerFAB = rootView.findViewById(R.id.datePickerFAB);
		datePickerContainer = rootView.findViewById(R.id.datePickerContainer);
		datePickerFAB.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				toggleDatePicker(view);
			}
		});

		View dateGeneral = rootView.findViewById(R.id.date_general);
		dateGeneral.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onDateInvoke(new Schedule(fromId, toId, null), view);
			}
		});
		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		datePickerContainer.setVisibility(View.INVISIBLE);
		dateAdapter.updateDataSet(composeDataSet());
	}

	public List<List<Schedule>> composeDataSet() {
		// Fetch from database
		Schedule[] savedSchedules = Database
				.getInstance(getContext())
				.getSchedules(new ScheduleQuery(fromId, toId, null).noData());

		// Compose ArrayList of saved schedules and today
		List<List<Schedule>> dataSet = new ArrayList<>(2);
		dataSet.add(new ArrayList<Schedule>(savedSchedules.length));
		dataSet.add(new ArrayList<Schedule>(savedSchedules.length));
		Schedule today = new Schedule(fromId, toId, Formatter.toISODate(Calendar.getInstance()));
		boolean todayFound = false;
		for (Schedule schedule : savedSchedules) {
			todayFound = todayFound || schedule.date.compareTo(today.date) == 0;
			int bundle = schedule.date.compareTo(today.date) < 0 ? 0 : 1;

			if (bundle == 1 && !todayFound) {
				dataSet.get(bundle).add(today);
				todayFound = true;
			}

			dataSet.get(bundle).add(schedule);
		}
		if (!todayFound)
			dataSet.get(1).add(today);
		return dataSet;
	}

	public boolean onBackPressed() {
		if (datePickerContainer.getVisibility() == View.VISIBLE) {
			toggleDatePicker(datePickerFAB);
			return true;
		}
		return false;
	}

	private void toggleDatePicker(View actorView) {
		boolean hidden = datePickerContainer.getVisibility() != View.VISIBLE;

		if (hidden) {
			calendarView.setCurrentDate(Calendar.getInstance());
		}

		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
			datePickerContainer.setVisibility(hidden ? View.VISIBLE : View.INVISIBLE);
		} else {

			int cx = (int) (actorView.getX() + actorView.getWidth() / 2);
			int cy = (int) (actorView.getY() + actorView.getHeight() / 2);

			float initialRadius = hidden ? 0 : (float) Math.hypot(cx, cy);
			float finalRadius = hidden ? (float) Math.hypot(cx, cy) : 0;

			Animator anim = ViewAnimationUtils.createCircularReveal(datePickerContainer, cx, cy, initialRadius, finalRadius);
			anim.setDuration(500);
			anim.setInterpolator(new AccelerateDecelerateInterpolator());
			if (hidden)
				datePickerContainer.setVisibility(View.VISIBLE);
			else
				anim.addListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						super.onAnimationEnd(animation);
						datePickerContainer.setVisibility(View.INVISIBLE);
					}
				});
			anim.start();
		}
	}

	void removeSchedule(final BundleAdapter.ItemViewHolder holder) {
		Schedule schedule = (Schedule) holder.object;
		if (schedule.isSaved()) {
			ScheduleQuery query = new ScheduleQuery(schedule);
			final Schedule scheduleWithData = Database.getInstance(getContext()).getSchedule(query);
			final int bundle = dateAdapter.getBundle(holder.getAdapterPosition());
			Database.getInstance(getContext()).remove(query);
			dateAdapter.removeItem(holder.getAdapterPosition());

			// Undo remove
			Snackbar.make(rootView, R.string.snackbar_schedule_deleted, Snackbar.LENGTH_LONG).setAction(R.string.action_undo, new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					Database.getInstance(getContext()).save(scheduleWithData);
					dateAdapter.addItem(scheduleWithData, bundle);
				}
			}).show();
		} else {
			dateAdapter.notifyItemChanged(holder.getAdapterPosition());
		}
	}

	void removePastSchedules() {
		// remove from database
		ScheduleQuery query = new ScheduleQuery(fromId, toId, null).beforeToday();
		final Schedule schedules[] = Database.getInstance(getContext()).getSchedules(query);
		Database.getInstance(getContext()).remove(query);

		// remove from recycler view
		dateAdapter.removeBundle(0);

		// Undo remove snack bar
		Snackbar.make(rootView, R.string.snackbar_past_schedules_deleted, Snackbar.LENGTH_LONG).setAction(R.string.action_undo, new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Database.getInstance(getContext()).save(schedules);
				dateAdapter.addItems(schedules, 0);
			}
		}).show();
	}

	private Intent composeScheduleIntent(Schedule schedule) {
		Intent intent = new Intent(getContext(), ScheduleActivity.class);
		intent.putExtras(getActivity().getIntent());
		if (!schedule.isGeneral())
			intent.putExtra(ARG_DATE, schedule.date);
		return intent;
	}

	public void onDateInvoke(Schedule schedule, View dateTitleView) {
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
			startActivity(composeScheduleIntent(schedule));
		} else {
			ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(), dateTitleView, "date_title_" + schedule.date);
			startActivity(composeScheduleIntent(schedule), options.toBundle());
		}
	}

	@Override
	public void onItemClick(BundleAdapter.ItemViewHolder viewHolder) {
		onDateInvoke((Schedule) viewHolder.object, viewHolder.view.findViewById(R.id.title));
	}

	class DateItemSwipeCallbackHelper extends ItemTouchHelper.SimpleCallback {
		DateItemSwipeCallbackHelper(int dragDirs, int swipeDirs) {
			super(dragDirs, swipeDirs);
		}

		@Override
		public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
			return false;
		}

		@Override
		public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
			if (!((BundleAdapter.ViewHolder) viewHolder).isBundleTitle())
				DateSelectorFragment.this.removeSchedule((BundleAdapter.ItemViewHolder) viewHolder);
			else if (((BundleAdapter.BundleViewHolder) viewHolder).bundle == 0)
				removePastSchedules();
			else
				dateAdapter.notifyItemChanged(viewHolder.getAdapterPosition());
		}
	}
}
