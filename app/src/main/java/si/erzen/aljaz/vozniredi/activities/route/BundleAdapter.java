package si.erzen.aljaz.vozniredi.activities.route;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import si.erzen.aljaz.vozniredi.R;

/**
 * Adapter for creating lists of items bundled into groups with titles.
 * Very general, can have custom binder, sorting, bundle titles.
 * <p>
 * Bundle titles hide when there is only one non-empty bundle. This is not ideal, as one can have
 * multiple empty bundles, all showing only title and no items.
 */
class BundleAdapter<T> extends RecyclerView.Adapter<BundleAdapter.ViewHolder> {

	private static final int TYPE_ITEM = 1, TYPE_BUNDLE = 2;

	private Context context;
	private int itemLayout;
	private Binder<T> binder;
	private Comparator<T> comparator;
	private ItemInteractionListener listener;

	private Object[] titles;

	private List<Object> list;
	private List<Integer> sizes;
	private int totalSize = 0;
	private List<Integer> starts;
	private boolean showBundleTitles;

	private List<Boolean> extended;
	private List<Boolean> disallowCollapsing;

	BundleAdapter(Context context, int itemLayout, Object[] titles, Binder<T> binder, Comparator<T> comparator, ItemInteractionListener listener) {
		this.context = context;
		this.itemLayout = itemLayout;
		this.titles = titles;
		this.binder = binder;
		this.comparator = comparator;
		this.listener = listener;
		disallowCollapsing = new AutoExtendingArrayList();
		extended = new AutoExtendingArrayList();
	}

	private BundleItem createBundleTitle(int bundle) {
		if (titles[bundle] instanceof String)
			return new BundleItem((String) titles[bundle]);
		return new BundleItem(context, (int) titles[bundle]);
	}

	int getBundle(int position) {
		for (int i = 0; i < sizes.size(); i++) {
			if (position < getBundleSize(i)) {
				return i;
			}
			position -= getBundleSize(i);
		}
		return -1;
	}

	private int getBundleSize(int bundle) {
		return (extended.get(bundle) ? sizes.get(bundle) : 0) + (showBundleTitles ? 1 : 0);
	}

	private int getBundleStart(int bundle) {
		if (bundle >= starts.size())
			return totalSize;
		return starts.get(bundle);
	}

	private int getIndex(int position) {
		int index = position;
		for (int i = 0; i < sizes.size() && (position > getBundleStart(i)); i++)
			if (!extended.get(i))
				index += sizes.get(i);
		return index;
	}

	private int getPosition(int index) {
		int position = index;
		for (int i = 0; i < sizes.size() && (position > getBundleStart(i)); i++)
			if (!extended.get(i))
				position -= sizes.get(i);
		return position;
	}


	private Object get(int position) {
		return list.get(getIndex(position));
	}

	private void toggleBundle(int bundle, View bundleArrow) {
		boolean bundleExtended = !extended.get(bundle);

		extended.set(bundle, bundleExtended);
		refreshBundleStarts();
		if (bundleExtended)
			notifyItemRangeInserted(getBundleStart(bundle) + (showBundleTitles ? 1 : 0), sizes.get(bundle));
		else
			notifyItemRangeRemoved(getBundleStart(bundle) + (showBundleTitles ? 1 : 0), sizes.get(bundle));

		if (bundleArrow != null)
			bundleArrow.animate().rotation(bundleExtended ? 0 : 90).start();
	}

	// Does not force update or animate
	void setExtended(int bundle, boolean bundleExtended) {
		extended.set(bundle, bundleExtended);
	}

	void disallowCollapsing(int bundle) {
		disallowCollapsing.set(bundle, true);
	}

	private void refreshBundleStarts() {
		starts = new ArrayList<>(sizes.size());
		int index = 0, numOfNonEmpty = 0;
		for (int i = 0; i < sizes.size(); i++) {
			starts.add(index);
			index += getBundleSize(i);
			if (sizes.get(i) > 0)
				numOfNonEmpty++;
		}
		boolean newShowBundleTitles = numOfNonEmpty >= 2;
		totalSize = index;

		if (newShowBundleTitles != showBundleTitles) {
			showBundleTitles = newShowBundleTitles;
			index = 0;
			if (newShowBundleTitles) {
				// Add bundle titles
				for (int i = 0; i < sizes.size(); i++) {
					list.add(index, createBundleTitle(i));
					notifyItemInserted(getPosition(index));
					index += sizes.get(i) + 1;
				}
			} else {
				// Remove bundle titles
				for (int i = 0; i < sizes.size(); i++) {
					list.remove(index);
					notifyItemRemoved(getPosition(index));
					index += sizes.get(i);
				}
				for (int i = 0; i < sizes.size(); i++) {
					if (sizes.get(i) > 0 && !extended.get(i))
						toggleBundle(i, null);
				}
			}
			refreshBundleStarts();
		}
	}

	private void setDataSet(List<List<T>> dataSet) {
		sizes = new ArrayList<>(dataSet.size());

		int numOfNonEmpty = 0, totalItemCount = 0;
		for (List<T> l : dataSet) {
			if (l.size() > 0)
				numOfNonEmpty++;
			totalItemCount += l.size();
			sizes.add(l.size());
			extended.add(true);
		}
		showBundleTitles = numOfNonEmpty >= 2;
		refreshBundleStarts();
		list = new ArrayList<>(totalItemCount + (showBundleTitles ? numOfNonEmpty : 0));

		int i = 0;
		for (List<T> l : dataSet) {
			if (showBundleTitles) {
				list.add(createBundleTitle(i++));
			}
			for (T t : l) {
				list.add(t);
			}
		}
	}

	void updateDataSet(List<List<T>> dataSet) {
		setDataSet(dataSet);
		notifyDataSetChanged();
	}

	void removeItem(int position) {
		if (getItemViewType(position) != TYPE_ITEM)
			return;

		int bundle = getBundle(position);
		list.remove(getIndex(position));
		notifyItemRemoved(position);

		sizes.set(bundle, sizes.get(bundle) - 1);
		refreshBundleStarts();
	}

	void removeBundle(int bundle) {
		int index = (showBundleTitles ? 1 : 0);
		for (int i = 0; i < bundle; i++)
			index += sizes.get(i) + (showBundleTitles ? 1 : 0);
		int size = sizes.get(bundle);

		list.subList(index, index + size).clear();
		if (extended.get(bundle))
			notifyItemRangeRemoved(getPosition(index), size);

		sizes.set(bundle, 0);
		refreshBundleStarts();
	}

	void addItem(T object, int bundle) {

		// Find index to insert object to
		// (can't use getBundleStart here, must not ignore hidden items)
		int index = (showBundleTitles ? 1 : 0);
		for (int i = 0; i < bundle; i++)
			index += sizes.get(i) + (showBundleTitles ? 1 : 0);
		int end = index + sizes.get(bundle);
		for (; index < end; index++) {
			Object item = list.get(index);
			//noinspection unchecked
			if (!(item instanceof BundleItem) && comparator.isBefore((T) item, object))
				break;
		}
		list.add(index, object);
		if (extended.get(bundle))
			notifyItemInserted(getPosition(index));
		sizes.set(bundle, sizes.get(bundle) + 1);
		refreshBundleStarts();
	}

	void addItems(T[] objects, int bundle) {
		int index = (showBundleTitles ? 1 : 0);
		for (int i = 0; i < bundle; i++)
			index += sizes.get(i) + (showBundleTitles ? 1 : 0);

		list.addAll(index, Arrays.asList(objects));
		sizes.set(bundle, sizes.get(bundle) + objects.length);
		if (extended.get(bundle))
			notifyItemRangeInserted(getPosition(index), objects.length);
		refreshBundleStarts();
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		if (viewType == TYPE_ITEM)
			return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false), listener);
		else
			return new BundleViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_bundle_title, parent, false));
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onBindViewHolder(final ViewHolder holder, int position) {
		Object item = get(position);
		if (getItemViewType(position) == TYPE_ITEM) {
			binder.bind(holder, (T) item);
			((ItemViewHolder) holder).object = item;
		} else if (getItemViewType(position) == TYPE_BUNDLE) {
			String title = ((BundleItem) item).text;
			((TextView) holder.view.findViewById(R.id.bundle_title)).setText(title);
			int bundle = getBundle(position);
			((BundleViewHolder) holder).bundle = bundle;

			View bundleArrow = holder.view.findViewById(R.id.bundle_arrow);
			if (disallowCollapsing.get(bundle)) {
				bundleArrow.setVisibility(View.GONE);
				holder.view.setOnClickListener(null);
			} else {
				bundleArrow.setVisibility(View.VISIBLE);
				bundleArrow.findViewById(R.id.bundle_arrow).setRotation(extended.get(bundle) ? 0 : 90);
				holder.view.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						toggleBundle(((BundleViewHolder) holder).bundle, holder.view.findViewById(R.id.bundle_arrow));
					}
				});
			}
		}
	}

	static class ViewHolder extends RecyclerView.ViewHolder {
		View view;

		ViewHolder(View itemView) {
			super(itemView);
			view = itemView;
		}

		boolean isBundleTitle() {
			return getItemViewType() == TYPE_BUNDLE;
		}
	}

	static class ItemViewHolder extends ViewHolder {
		Object object;

		ItemViewHolder(View itemView, final ItemInteractionListener listener) {
			super(itemView);
			itemView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					listener.onItemClick(ItemViewHolder.this);
				}
			});
		}
	}

	static class BundleViewHolder extends ViewHolder {
		int bundle;

		BundleViewHolder(View itemView) {
			super(itemView);
		}
	}

	private static class BundleItem {
		String text;

		BundleItem(String text) {
			this.text = text;
		}

		BundleItem(Context context, int resId) {
			text = context.getString(resId);
		}
	}

	@Override
	public int getItemViewType(int position) {
		return get(position) instanceof BundleItem ? TYPE_BUNDLE : TYPE_ITEM;
	}

	@Override
	public int getItemCount() {
		return totalSize;
	}

	interface ItemInteractionListener {
		void onItemClick(ItemViewHolder viewHolder);
	}

	interface Comparator<T> {
		boolean isBefore(T a, T b);
	}

	interface Binder<T> {
		void bind(ViewHolder holder, T object);
	}

	private static class AutoExtendingArrayList extends ArrayList<Boolean> {
		@Override
		public Boolean set(int index, Boolean element) {
			while (index >= size())
				add(false);
			return super.set(index, element);
		}

		@Override
		public Boolean get(int index) {
			if (index >= size())
				return false;
			return super.get(index);
		}
	}
}
