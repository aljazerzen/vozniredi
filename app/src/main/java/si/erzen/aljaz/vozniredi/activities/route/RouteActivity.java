package si.erzen.aljaz.vozniredi.activities.route;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import si.erzen.aljaz.vozniredi.R;
import si.erzen.aljaz.vozniredi.backend.Database;
import si.erzen.aljaz.vozniredi.backend.Schedule;
import si.erzen.aljaz.vozniredi.backend.ScheduleQuery;

import static si.erzen.aljaz.vozniredi.util.Constants.ARG_FROM_ID;
import static si.erzen.aljaz.vozniredi.util.Constants.ARG_TO_ID;
import static si.erzen.aljaz.vozniredi.util.Constants.PREF_DELETE_PAST;

public class RouteActivity extends AppCompatActivity {

	long fromId, toId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_route);

		fromId = getIntent().getLongExtra(ARG_FROM_ID, -1);
		toId = getIntent().getLongExtra(ARG_TO_ID, -1);
		if (fromId < 0 || toId < 0) {
			finish();
			return;
		}

		Schedule schedule = new Schedule(fromId, toId);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		if (getSupportActionBar() != null) {
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setTitle(schedule.getRoute(this));
		}

		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		if (sharedPref.getBoolean(PREF_DELETE_PAST, false)) {
			Database.getInstance(this).remove(new ScheduleQuery(schedule).beforeToday());
		}
	}

	@Override
	protected void onStart() {
		super.onStart();

		DateSelectorFragment fragment = (DateSelectorFragment) getSupportFragmentManager().findFragmentById(R.id.date_selector_fragment);
		if (fragment != null)
			fragment.setRoute(fromId, toId);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_route, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_reverse) {
			Intent intent = new Intent(this, RouteActivity.class);
			intent.putExtra(ARG_FROM_ID, toId);
			intent.putExtra(ARG_TO_ID, fromId);
			startActivity(intent);
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		DateSelectorFragment fragment = (DateSelectorFragment) getSupportFragmentManager().findFragmentById(R.id.date_selector_fragment);
		if (fragment != null && fragment.onBackPressed()) {
			// Date picker has been closed. Do not issue back action.
			return;
		}
		super.onBackPressed();
	}


}
