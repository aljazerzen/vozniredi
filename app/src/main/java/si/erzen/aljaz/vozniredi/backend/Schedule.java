package si.erzen.aljaz.vozniredi.backend;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import si.erzen.aljaz.vozniredi.R;
import si.erzen.aljaz.vozniredi.util.Formatter;

import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_ARRIVAL;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_DEPARTURE;

@SuppressWarnings("WeakerAccess")
public class Schedule {

	public long fromId;
	public long toId;
	public String date;
	public String data;
	public long lastUpdate;

	// Cache
	private String dateTitle;
	private String dateSubtitle;
	private String route;
	private ArrayList<ScheduleDataSetItem> dataSet;

	private boolean saved = false;

	public boolean isSaved() {
		return saved;
	}

	private Schedule(Cursor c) {
		saved = true;
		int index;

		index = c.getColumnIndex(Database.COLUMN_FROM_ID);
		if (index != -1 && !c.isNull(index))
			fromId = c.getLong(index);

		index = c.getColumnIndex(Database.COLUMN_TO_ID);
		if (index != -1 && !c.isNull(index))
			toId = c.getLong(index);

		index = c.getColumnIndex(Database.COLUMN_DATE);
		date = index != -1 && !c.isNull(index) ? c.getString(index) : null;

		index = c.getColumnIndex(Database.COLUMN_DATA);
		data = index != -1 && !c.isNull(index) ? c.getString(index) : "[]";

		index = c.getColumnIndex(Database.COLUMN_LAST_UPDATE);
		if (index != -1 && !c.isNull(index))
			lastUpdate = c.getLong(index);
	}

	/**
	 * Creates empty general schedule on route
	 */
	public Schedule(long fromId, long toId) {
		this(fromId, toId, null);
	}

	/**
	 * Creates empty day specific schedule on route
	 */
	public Schedule(long fromId, long toId, String date) {
		this.fromId = fromId;
		this.toId = toId;
		this.date = date;
	}

	static Schedule[] loadFromCursor(Cursor c) {
		Schedule[] r = new Schedule[c.getCount()];

		int index = 0;
		c.moveToFirst();
		while (!c.isAfterLast()) {
			r[index++] = new Schedule(c);
			c.moveToNext();
		}

		return r;
	}

	ContentValues genContentValues() {
		saved = true;
		ContentValues values = new ContentValues();
		values.put(Database.COLUMN_FROM_ID, fromId);
		values.put(Database.COLUMN_TO_ID, toId);
		if (!isGeneral())
			values.put(Database.COLUMN_DATE, date);
		values.put(Database.COLUMN_DATA, data);
		values.put(Database.COLUMN_LAST_UPDATE, lastUpdate);
		return values;
	}

	ScheduleQuery getQuery() {
		return new ScheduleQuery(this);
	}

	public boolean isGeneral() {
		return date == null;
	}

	private void genDateTitles(Context context) {
		if (isGeneral()) {
			dateTitle = context.getString(R.string.general);
			dateSubtitle = "";
			return;
		}

		Date date = Formatter.fromISODate(this.date);
		Locale locale = Formatter.getCurrentLocale(context);

		DateFormat dateFormatDay = new SimpleDateFormat("EEEE", locale);
		DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, locale);
		dateSubtitle = dateFormat.format(date);

		long daysTo = TimeUnit.DAYS.convert(date.getTime(), TimeUnit.MILLISECONDS)
				- TimeUnit.DAYS.convert(Calendar.getInstance().getTimeInMillis(), TimeUnit.MILLISECONDS);
		if (daysTo == 0) {
			dateTitle = context.getString(R.string.today);
		} else if (daysTo == 1) {
			dateTitle = context.getString(R.string.tomorrow);
		} else if (daysTo == -1) {
			dateTitle = context.getString(R.string.yesterday);
		} else {
			dateTitle = dateFormatDay.format(date);

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			Calendar today = Calendar.getInstance();
			if (today.get(Calendar.YEAR) == calendar.get(Calendar.YEAR)) {
				calendar.setFirstDayOfWeek(Calendar.MONDAY);
				today.setFirstDayOfWeek(Calendar.MONDAY);
				int weeksTo = calendar.get(Calendar.WEEK_OF_YEAR) - Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);
				if (weeksTo > 0) {
					dateTitle += " " + context.getResources().getQuantityString(R.plurals.weeksFromNow, weeksTo, weeksTo);
				} else if (weeksTo < 0) {
					weeksTo = -weeksTo;
					dateTitle += " " + context.getResources().getQuantityString(R.plurals.weeksAgo, weeksTo, weeksTo);
				}
			}
		}
		dateTitle = dateTitle.substring(0, 1).toUpperCase() + dateTitle.substring(1);
	}

	public String getDateTitle(Context context) {
		if (dateTitle == null)
			genDateTitles(context);
		return dateTitle;
	}

	public String getDateSubtitle(Context context) {
		if (dateSubtitle == null)
			genDateTitles(context);
		return dateSubtitle;
	}

	public List<ScheduleDataSetItem> getDataSet(Context context) {
		if (dataSet == null) {
			try {
				JSONArray jsonArray = new JSONArray(data);
				dataSet = new ArrayList<>(jsonArray.length());

				for (int i = 0, l = jsonArray.length(); i < l; i++) {
					Drive drive = new Drive(jsonArray.getJSONObject(i)).addTTDField(date);
					dataSet.add(drive);
				}
			} catch (JSONException e) {
				Log.e("schedule-storage", "JSON parse error: " + e.toString());
			}
		}
		return dataSet;
	}

	public List<ScheduleDataSetItem> composeDataSetWithPinnedReminders(Context context, PinnedDrive[] pinnedDrives) {
		getDataSet(context);
		if (isGeneral())
			return dataSet;

		List<ScheduleDataSetItem> list = new ArrayList<>(dataSet.size());

		List<PinnedDriveReminder> reminders = PinnedDriveReminder.filter(pinnedDrives, this);

		int piArriving = 0, piDeparting = 0;
		for (ScheduleDataSetItem drive : dataSet) {
			while (piArriving < reminders.size() && reminders.get(piArriving).time.compareTo(((Drive) drive).get(FIELD_DEPARTURE)) <= 0) {
				if (reminders.get(piArriving).arriving)
					list.add(reminders.get(piArriving));
				piArriving++;
			}

			while (piDeparting < reminders.size() && reminders.get(piDeparting).time.compareTo(((Drive) drive).get(FIELD_ARRIVAL)) <= 0) {
				if (!reminders.get(piDeparting).arriving)
					list.add(reminders.get(piDeparting));
				piDeparting++;
			}
			list.add(drive);
		}
		return list;
	}

	// Cached
	public String getRoute(Context context) {
		if (route == null) {
			Stations stations = Stations.getInstance(context);
			route = context.getResources().getString(R.string.route, stations.get(fromId).getLabel(), stations.get(toId).getLabel());
		}
		return route;
	}

	@Override
	public String toString() {
		return "Schedule{" +
				"lastUpdate=" + lastUpdate +
				", data='" + data + '\'' +
				", date='" + date + '\'' +
				", toId=" + toId +
				", fromId=" + fromId +
				'}';
	}
}
