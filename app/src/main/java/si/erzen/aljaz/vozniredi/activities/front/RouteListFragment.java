package si.erzen.aljaz.vozniredi.activities.front;

import android.animation.Animator;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import si.erzen.aljaz.vozniredi.R;
import si.erzen.aljaz.vozniredi.activities.route.RouteActivity;
import si.erzen.aljaz.vozniredi.backend.Database;
import si.erzen.aljaz.vozniredi.backend.Schedule;
import si.erzen.aljaz.vozniredi.backend.ScheduleQuery;

import static android.app.Activity.RESULT_OK;
import static si.erzen.aljaz.vozniredi.util.Constants.ARG_FROM_ID;
import static si.erzen.aljaz.vozniredi.util.Constants.ARG_TO_ID;

public class RouteListFragment extends Fragment implements View.OnClickListener, OnRouteClickListener {

	RouteListAdapter adapter;
	View rootView;

	// Add route drawer: views and data
	View inputContainer;
	EditText fromInput, toInput;
	long fromId = -1, toId = -1;

	public RouteListFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_routes, container, false);

		// Set the adapter
		RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.list);
		recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
		adapter = new RouteListAdapter(loadRoutes(), this);
		recyclerView.setAdapter(adapter);

		// ---- [ Route add setup ] ----

		// Input container toggling
		rootView.findViewById(R.id.add_route_fab).setOnClickListener(this);
		rootView.findViewById(R.id.button_add).setOnClickListener(this);
		rootView.findViewById(R.id.button_cancel).setOnClickListener(this);
		inputContainer = rootView.findViewById(R.id.route_input_container);

		fromInput = ((EditText) rootView.findViewById(R.id.fromInput));
		fromInput.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				StationSearchFragment fragment = StationSearchFragment.newInstance(fromInput.getText().toString());
				fragment.setTargetFragment(RouteListFragment.this, StationSearchFragment.REQUEST_CODE_FROM);
				fragment.show(getFragmentManager());
			}
		});
		toInput = ((EditText) rootView.findViewById(R.id.toInput));
		toInput.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				StationSearchFragment fragment = StationSearchFragment.newInstance(toInput.getText().toString());
				fragment.setTargetFragment(RouteListFragment.this, StationSearchFragment.REQUEST_CODE_TO);
				fragment.show(getFragmentManager());
			}
		});

		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		adapter.updateDataSet(loadRoutes());
		if (adapter.getItemCount() == 0)
			openAddRouteDrawer();
	}

	Schedule[] loadRoutes() {
		return Database.getInstance(getContext()).getSchedules(new ScheduleQuery().groupByRoute());
	}

	private void openAddRouteDrawer() {
		inputContainer.setVisibility(View.VISIBLE);
		inputContainer.setTranslationY(inputContainer.getHeight());
		inputContainer.animate().translationY(0).start();
	}

	private void closeAddRouteDrawer() {
		inputContainer.animate().translationY(inputContainer.getHeight()).setListener(new Animator.AnimatorListener() {
			@Override
			public void onAnimationStart(Animator animator) {

			}

			@Override
			public void onAnimationEnd(Animator animator) {
				fromInput.setText("");
				fromId = -1;
				toInput.setText("");
				toId = -1;
			}

			@Override
			public void onAnimationCancel(Animator animator) {

			}

			@Override
			public void onAnimationRepeat(Animator animator) {

			}
		}).start();
	}

	@Override
	public void onRouteInvoke(Schedule route) {
		Intent intent = new Intent(getContext(), RouteActivity.class);
		intent.putExtra(ARG_FROM_ID, route.fromId);
		intent.putExtra(ARG_TO_ID, route.toId);
		startActivity(intent);
	}

	@Override
	public void onRouteDelete(Schedule route, final int position) {
		ScheduleQuery scheduleQuery = new ScheduleQuery(route).allOnRoute();
		final Schedule[] schedules = Database.getInstance(getContext()).getSchedules(scheduleQuery);
		Database.getInstance(getContext()).remove(scheduleQuery);
		adapter.deleteItem(position);

		Snackbar.make(rootView, R.string.snackbar_route_deleted, Snackbar.LENGTH_LONG).setAction(R.string.action_undo, new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Database.getInstance(getContext()).save(schedules);
				adapter.addItem(schedules[0], position);
			}
		}).show();
	}

	/* Route Add Methods */

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.add_route_fab:
				openAddRouteDrawer();
				break;
			case R.id.button_cancel:
				closeAddRouteDrawer();
				break;
			case R.id.button_add:
				add();
				break;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == StationSearchFragment.REQUEST_CODE_TO || requestCode == StationSearchFragment.REQUEST_CODE_FROM) {
			if (resultCode == RESULT_OK) {
				long stationId = data.getLongExtra(StationSearchFragment.ARG_STATION_ID, 0);
				String stationName = data.getStringExtra(StationSearchFragment.ARG_STATION);

				if (requestCode == StationSearchFragment.REQUEST_CODE_FROM) {
					fromInput.setText(stationName);
					fromId = stationId;
				} else {
					toInput.setText(stationName);
					toId = stationId;
				}
			}
		}
	}

	public void add() {
		if (fromId == -1 || toId == -1)
			return;

		closeAddRouteDrawer();
		onRouteInvoke(new Schedule(fromId, toId));
	}
}
