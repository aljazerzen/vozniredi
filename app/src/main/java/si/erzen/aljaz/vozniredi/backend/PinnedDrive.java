package si.erzen.aljaz.vozniredi.backend;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;

import org.json.JSONException;
import org.json.JSONObject;

import si.erzen.aljaz.vozniredi.activities.front.FrontActivity;
import si.erzen.aljaz.vozniredi.activities.pinned.AlarmActivity;
import si.erzen.aljaz.vozniredi.util.Formatter;

import static si.erzen.aljaz.vozniredi.util.Constants.ARG_PINNED_ID;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_ARRIVAL;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_DEPARTURE;

public class PinnedDrive {

	long id = -1;
	Schedule schedule;
	private Drive drive;
	private long alarmTimestamp = -1;

	public PinnedDrive(Schedule schedule, Drive drive) {
		this.schedule = schedule;
		this.drive = drive;
	}

	private PinnedDrive(Cursor c) {
		int index;
		long fromId = 0, toId = 0;

		index = c.getColumnIndex(Database.COLUMN_ID);
		if (index != -1 && !c.isNull(index))
			id = c.getInt(index);

		index = c.getColumnIndex(Database.COLUMN_FROM_ID);
		if (index != -1 && !c.isNull(index))
			fromId = c.getLong(index);

		index = c.getColumnIndex(Database.COLUMN_TO_ID);
		if (index != -1 && !c.isNull(index))
			toId = c.getLong(index);

		index = c.getColumnIndex(Database.COLUMN_DATE);
		String date = index != -1 && !c.isNull(index) ? c.getString(index) : null;
		schedule = new Schedule(fromId, toId, date);

		index = c.getColumnIndex(Database.COLUMN_DATA);
		String data = index != -1 && !c.isNull(index) ? c.getString(index) : "[]";
		try {
			drive = new Drive(new JSONObject(data)).addTTDField(date);
		} catch (JSONException ignored) {
			ignored.printStackTrace();
		}

		index = c.getColumnIndex(Database.COLUMN_ALARM);
		if (index != -1 && !c.isNull(index))
			alarmTimestamp = c.getLong(index);
	}

	static PinnedDrive[] loadFromCursor(Cursor c) {
		PinnedDrive[] drives = new PinnedDrive[c.getCount()];
		int index = 0;
		c.moveToFirst();
		while (!c.isAfterLast()) {
			drives[index++] = new PinnedDrive(c);
			c.moveToNext();
		}
		return drives;
	}

	public long getId() {
		return id;
	}

	public void clearId() {
		this.id = -1;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	public Drive getDrive() {
		return drive;
	}

	public long getAlarmTimestamp() {
		return alarmTimestamp;
	}

	ContentValues genContentValues() {
		ContentValues values = new ContentValues();
		values.put(Database.COLUMN_FROM_ID, schedule.fromId);
		values.put(Database.COLUMN_TO_ID, schedule.toId);
		if (!schedule.isGeneral())
			values.put(Database.COLUMN_DATE, schedule.date);
		values.put(Database.COLUMN_DATA, drive.toString());
		values.put(Database.COLUMN_ALARM, alarmTimestamp);
		return values;
	}

	PinnedDrive findItself(PinnedDrive[] pinnedDrives) {
		for (PinnedDrive drive : pinnedDrives) {
			if (drive.equals(this))
				return drive;
		}
		return null;
	}

	public boolean equals(PinnedDrive pinned) {
		if (id != -1 && pinned.id != -1)
			return pinned.id == id;
		return pinned.schedule.fromId == schedule.fromId &&
				pinned.schedule.toId == schedule.toId &&
				pinned.schedule.date.equals(schedule.date) &&
				pinned.drive.get(FIELD_DEPARTURE).equals(drive.get(FIELD_DEPARTURE)) &&
				pinned.drive.get(FIELD_ARRIVAL).equals(drive.get(FIELD_ARRIVAL));
	}

	public long getDepartureDatetime() {
		return Formatter.fromScheduleTime(drive.get(FIELD_DEPARTURE)).getTime() +
				Formatter.fromISODate(schedule.date).getTime();
	}

	public boolean setAlarmBeforeDeparture(Context context, int minBeforeDeparture) {
		return setAlarm(context, getDepartureDatetime() - minBeforeDeparture * 60 * 1000);
	}

	public boolean setAlarm(Context context, long alarmTimestamp) {
		if (alarmTimestamp < System.currentTimeMillis())
			return false;

		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

		Intent alarmIntent = new Intent(context, AlarmActivity.class);
		alarmIntent.putExtra(ARG_PINNED_ID, id);
		PendingIntent pendingAlarmIntent = PendingIntent.getActivity(context, (int) id, alarmIntent, 0);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Intent showAlarmIntent = new Intent(context, FrontActivity.class);
			showAlarmIntent.putExtra(ARG_PINNED_ID, id);
			PendingIntent pendingShowAlarmIntent = PendingIntent.getActivity(context, 42, showAlarmIntent, 0);

			alarmManager.setAlarmClock(new AlarmManager.AlarmClockInfo(alarmTimestamp, pendingShowAlarmIntent), pendingAlarmIntent);
		} else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTimestamp, pendingAlarmIntent);
		} else {
			alarmManager.set(AlarmManager.RTC_WAKEUP, alarmTimestamp, pendingAlarmIntent);
		}

		this.alarmTimestamp = alarmTimestamp;
		return true;
	}

	public void removeAlarm(Context context) {
		PendingIntent pendingIntent = PendingIntent.getActivity(context, (int) id, new Intent(context, AlarmActivity.class), 0);
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(pendingIntent);
		pendingIntent.cancel();
		alarmTimestamp = -1;
	}

	public boolean hasAlarmSet() {
		return alarmTimestamp > System.currentTimeMillis();
	}
}
