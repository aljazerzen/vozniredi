package si.erzen.aljaz.vozniredi.util;

import java.util.HashMap;
import java.util.Map;

import si.erzen.aljaz.vozniredi.R;

public class Constants {

	public static final String ARG_FROM_ID = "fromId";
	public static final String ARG_TO_ID = "toId";
	public static final String ARG_DATE = "date";
	public static final String ARG_PINNED_ID = "pinned_id";
	public static final String ARG_ALARM_MIN = "alarm_min";

	public static final String FIELD_DEPARTURE = "dep";
	public static final String FIELD_ARRIVAL = "arr";

//	public static final String FIELD_PLATFORM = "pla";
//	public static final String FIELD_PRICE = "pri";
//	public static final String FIELD_DISTANCE = "dis";

	public static final String FIELD_DURATION = "dur";
	public static final String FIELD_REGIME = "reg";
	public static final String FIELD_TTD = "ttd";
	public static final String FIELD_REGIME_LONG = "rel";
	public static final String FIELD_OPERATOR = "rid";

	public static final Map<String, Integer> FIELD_STRING_IDS;

	public static final int REQUEST_CODE_BUY_FULL = 0x81;

	static {
		FIELD_STRING_IDS = new HashMap<>();
		FIELD_STRING_IDS.put(FIELD_DEPARTURE, R.string.field_departure);
		FIELD_STRING_IDS.put(FIELD_ARRIVAL, R.string.field_arrival);
//		FIELD_STRING_IDS.put(FIELD_PLATFORM, R.string.field_platform);
//		FIELD_STRING_IDS.put(FIELD_PRICE, R.string.field_price);
//		FIELD_STRING_IDS.put(FIELD_DISTANCE, R.string.field_distance);
		FIELD_STRING_IDS.put(FIELD_DURATION, R.string.field_duration);
		FIELD_STRING_IDS.put(FIELD_REGIME, R.string.field_regime);
		FIELD_STRING_IDS.put(FIELD_TTD, R.string.field_ttd);
		FIELD_STRING_IDS.put(FIELD_REGIME_LONG, R.string.field_regime_long);
		FIELD_STRING_IDS.put(FIELD_OPERATOR, R.string.field_operator);
	}

	public static final String PREF_DELETE_PAST = "pref_delete_past";
	public static final String PREF_AUTO_DOWNLOAD = "pref_auto_download";
	public static final String PREF_SNOOZE_DURATION = "pref_snooze_duration";
	public static final String PREF_NOTIFICATIONS = "pref_notifications";
	public static final String PREF_SHARE_MESSAGE = "pref_share_message";

	public static final String PREF_RECENT = "pref_recent";
	public static final String PREF_RECENT_STATIONS = "pref_recent_stations";
	public static final int MAX_RECENT_STATIONS = 10;
}
