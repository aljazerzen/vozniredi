package si.erzen.aljaz.vozniredi.backend;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Calendar;

import si.erzen.aljaz.vozniredi.util.Formatter;

@SuppressWarnings({"unused", "WeakerAccess"})
public class Database extends SQLiteOpenHelper {

	/* Static Declarations */

	private static final int DATABASE_VERSION = 7;
	private static final String DATABASE_NAME = "vozniredi.db";

	static final String COLUMN_NULLABLE = "null";
	static final String COLUMN_ID = "id";
	static final String COLUMN_FROM_ID = "fromid";
	static final String COLUMN_TO_ID = "toid";
	static final String COLUMN_DATE = "date";
	static final String COLUMN_LAST_UPDATE = "lastupdate";
	static final String COLUMN_DATA = "data";
	static final String COLUMN_ALARM = "alarm";

	private static final String TABLE_SCHEDULES = "schedules";
	private static final String SQL_CREATE_SCHEDULES =
			"CREATE TABLE " + TABLE_SCHEDULES + " ( " +
					COLUMN_FROM_ID + " INTEGER," +
					COLUMN_TO_ID + " INTEGER," +
					COLUMN_DATE + " TEXT," +
					COLUMN_LAST_UPDATE + " INTEGER," +
					COLUMN_DATA + " TEXT," +
					"PRIMARY KEY(" + COLUMN_FROM_ID + "," + COLUMN_TO_ID + "," + COLUMN_DATE + "));";
	private static final String SQL_DELETE_SCHEDULES = "DROP TABLE IF EXISTS " + TABLE_SCHEDULES;

	private static final String TABLE_PINNED = "pinned";
	private static final String SQL_CREATE_PINNED =
			"CREATE TABLE " + TABLE_PINNED + " ( " +
					COLUMN_ID + " INTEGER PRIMARY KEY," +
					COLUMN_FROM_ID + " INTEGER," +
					COLUMN_TO_ID + " INTEGER," +
					COLUMN_DATE + " TEXT," +
					COLUMN_DATA + " TEXT," +
					COLUMN_ALARM + " INTEGER);";

	private static final String SQL_DELETE_PINNED = "DROP TABLE IF EXISTS " + TABLE_PINNED;

	private static final String WHERE_ID = COLUMN_ID + "= ?";
	private static final String WHERE_DATE_LT = COLUMN_DATE + "< ?";
	static final String WHERE_ROUTE_ID_GENERAL = COLUMN_FROM_ID + "= ? AND " + COLUMN_TO_ID + "= ? AND " + COLUMN_DATE + " IS NULL";
	static final String WHERE_ROUTE_ID_DATE = COLUMN_FROM_ID + "= ? AND " + COLUMN_TO_ID + "= ? AND " + COLUMN_DATE + "= ?";
	static final String WHERE_ROUTE_ID_DATE_LT = COLUMN_FROM_ID + "= ? AND " + COLUMN_TO_ID + "= ? AND " + COLUMN_DATE + "< ?";
	static final String WHERE_ROUTE_ID = COLUMN_FROM_ID + "= ? AND " + COLUMN_TO_ID + "= ? AND " + COLUMN_DATE + " IS NOT NULL";
	static final String WHERE_ROUTE_ALL = COLUMN_FROM_ID + "= ? AND " + COLUMN_TO_ID + "= ?";

	static final String GROUP_BY_ROUTE = COLUMN_FROM_ID + ", " + COLUMN_TO_ID;

	static final String[] SCHEDULES_FIELDS_ALL = new String[]{COLUMN_FROM_ID, COLUMN_TO_ID, COLUMN_DATE, COLUMN_LAST_UPDATE, COLUMN_DATA};
	static final String[] SCHEDULES_FIELDS_NO_DATA = new String[]{COLUMN_FROM_ID, COLUMN_TO_ID, COLUMN_DATE, COLUMN_LAST_UPDATE};
	static final String[] SCHEDULES_FIELDS_ROUTE = new String[]{COLUMN_FROM_ID, COLUMN_TO_ID, "COUNT(*)"};

	private static final String[] PINNED_FIELDS_ALL = new String[]{COLUMN_ID, COLUMN_FROM_ID, COLUMN_TO_ID, COLUMN_DATE, COLUMN_DATA, COLUMN_ALARM};
	private static final String PINNED_ORDER = COLUMN_DATE + "," + COLUMN_DATA;

	/* Singleton and constructor */

	private static Database singleton = null;

	private Database(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public static synchronized Database getInstance(Context context) {
		if (singleton == null)
			singleton = new Database(context.getApplicationContext());
		return singleton;
	}

	/* SQLite Helper Methods */

	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_SCHEDULES);
		db.execSQL(SQL_CREATE_PINNED);
	}

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(SQL_DELETE_SCHEDULES);
		db.execSQL(SQL_DELETE_PINNED);
		onCreate(db);
	}

	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onUpgrade(db, oldVersion, newVersion);
	}

	/* Actions Schedules */

	private void insert(Schedule e) {
		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(TABLE_SCHEDULES, COLUMN_NULLABLE, e.genContentValues());
	}

	private int update(Schedule e) {
		SQLiteDatabase db = this.getWritableDatabase();
		ScheduleQuery q = e.getQuery();
		return db.update(TABLE_SCHEDULES, e.genContentValues(), q.getWhereQuery(), q.getWhereQueryData());
	}

	public void save(Schedule e) {
		if (update(e) == 0) {
			insert(e);
		}
	}

	public void save(Schedule[] schedules) {
		for (Schedule schedule : schedules)
			save(schedule);
	}

	public Schedule getSchedule(ScheduleQuery query) {
		Schedule[] schedules = getSchedules(query);
		return schedules.length > 0 ? schedules[0] : null;
	}

	public Schedule[] getSchedules(ScheduleQuery query) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(TABLE_SCHEDULES, query.getFields(), query.getWhereQuery(), query.getWhereQueryData(), query.getGroupByFields(), null, null);
		Schedule[] r = Schedule.loadFromCursor(c);
		c.close();
		return r;
	}

	public void remove(ScheduleQuery query) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_SCHEDULES, query.getWhereQuery(), query.getWhereQueryData());
	}

	/* Actions Pinned Drives */

	private void insert(PinnedDrive e) {
		SQLiteDatabase db = this.getWritableDatabase();
		e.id = db.insert(TABLE_PINNED, COLUMN_NULLABLE, e.genContentValues());
	}

	private void update(PinnedDrive e) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.update(TABLE_PINNED, e.genContentValues(), WHERE_ID, new String[]{"" + e.id});
	}

	public void save(PinnedDrive e) {
		if (e.id < 0) {
			PinnedDrive savedCopy = e.findItself(getPinnedDrives(WHERE_ROUTE_ID_DATE, new String[]{"" + e.schedule.fromId, "" + e.schedule.toId, e.schedule.date}));

			if (savedCopy == null)
				insert(e);
			else
				e.id = savedCopy.getId();
		} else {
			update(e);
		}
	}

	public PinnedDrive getPinnedDrive(long id) {
		PinnedDrive[] r = getPinnedDrives(WHERE_ID, new String[]{"" + id});
		return r == null || r.length == 0 ? null : r[0];
	}

	public PinnedDrive[] getPinnedDrives() {
		return getPinnedDrives(null, null);
	}

	private PinnedDrive[] getPinnedDrives(String whereQuery, String[] whereQueryData) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(TABLE_PINNED, PINNED_FIELDS_ALL, whereQuery, whereQueryData, null, null, PINNED_ORDER);
		PinnedDrive[] r = PinnedDrive.loadFromCursor(c);
		c.close();
		return r;
	}

	public int remove(long id) {
		SQLiteDatabase db = this.getWritableDatabase();
		return db.delete(TABLE_PINNED, WHERE_ID, new String[]{"" + id});
	}

	public int removePastPinnedDrives() {
		SQLiteDatabase db = this.getWritableDatabase();
		return db.delete(TABLE_PINNED, WHERE_DATE_LT, new String[]{"" + Formatter.toISODate(Calendar.getInstance())});
	}
}