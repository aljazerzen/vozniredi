package si.erzen.aljaz.vozniredi.util;

import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import si.erzen.aljaz.vozniredi.R;

public class Formatter {

	private static DateFormat getIsoDateFormat() {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		format.setTimeZone(TimeZone.getTimeZone("UTC"));
		return format;
	}

	private static DateFormat getIsoTimeFormat() {
		DateFormat format = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
		format.setTimeZone(TimeZone.getTimeZone("UTC"));
		return format;
	}

	private static long sloveniaOffsetCache = -1;
	/**
	 * Calculates offset of Slovenian time off UTC, including Daylight Saving Time
	 * (Should return 1 hour during winter and 2 during summer)
	 * <p>
	 * Is it complicated and should probably need refactoring? Yes.
	 * Does it work? Yes. So shut the fuck up. I am just being lazy. Refactor yourself. Bitch.
	 *
	 * Now caches! I am gonna call the shit out of this.
	 *
	 * @return time in milliseconds
	 */
	public static long getSloveniaOffset() {
		if (sloveniaOffsetCache != -1)
			return sloveniaOffsetCache;

		DateFormat format = getIsoTimeFormat();
		format.setTimeZone(TimeZone.getTimeZone("Europe/Ljubljana"));
		String slovenianTime = format.format(Calendar.getInstance().getTime());
		format.setTimeZone(TimeZone.getTimeZone("UTC"));
		String UTCTime = format.format(Calendar.getInstance().getTime());
		Date slovenianDate, UTCDate;
		try {
			slovenianDate = format.parse(slovenianTime);
			UTCDate = format.parse(UTCTime);
			sloveniaOffsetCache = slovenianDate.getTime() - UTCDate.getTime();
			return sloveniaOffsetCache;
		} catch (ParseException e) {
			return 0;
		}
	}

	public static String toISODate(Calendar calendar) {
		return getIsoDateFormat().format(calendar.getTime());
	}

	public static Date fromISODate(String isoDate) {
		try {
			return getIsoDateFormat().parse(isoDate);
		} catch (ParseException e) {
			return new Date();
		}
	}

	public static Date fromScheduleTime(String isoDate) {
		getSloveniaOffset();
		try {
			DateFormat dateFormat = getIsoTimeFormat();
			return new Date(dateFormat.parse(isoDate).getTime() - getSloveniaOffset());
		} catch (ParseException e) {
			return new Date();
		}
	}

	public static Locale getCurrentLocale(Context context) {
		context = context.getApplicationContext();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			return context.getResources().getConfiguration().getLocales().get(0);
		} else {
			//noinspection deprecation
			return context.getResources().getConfiguration().locale;
		}
	}

	public static int dpToPx(Context context, int dp) {
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
	}

	public static String ISOTimeToHumanElapsed(String start, String end, Context context) {
		try {
			Date dateStart = getIsoTimeFormat().parse(start);
			Date dateEnd = getIsoTimeFormat().parse(end);
			return diffToHumanElapsed(dateStart.getTime(), dateEnd.getTime(), context);
		} catch (ParseException e) {
			return "";
		}
	}

	public static String diffToHumanElapsed(long start, long end, Context context) {
		long diff = (end - start) / 1000 / 60;

		String r = (diff % 60) + context.getString(R.string.minutes);
		if (diff >= 60) {
			r = (diff / 60) + context.getString(R.string.hours) + " " + r;
		}
		return r;
	}
}
