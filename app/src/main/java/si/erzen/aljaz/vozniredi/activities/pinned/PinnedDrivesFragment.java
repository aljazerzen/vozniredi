package si.erzen.aljaz.vozniredi.activities.pinned;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import si.erzen.aljaz.vozniredi.R;
import si.erzen.aljaz.vozniredi.backend.Database;
import si.erzen.aljaz.vozniredi.backend.PinnedDrive;
import si.erzen.aljaz.vozniredi.backend.PinnedDriveCombined;
import si.erzen.aljaz.vozniredi.backend.Stations;

import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static si.erzen.aljaz.vozniredi.util.Constants.ARG_ALARM_MIN;
import static si.erzen.aljaz.vozniredi.util.Constants.ARG_PINNED_ID;
import static si.erzen.aljaz.vozniredi.util.Constants.PREF_SHARE_MESSAGE;

public class PinnedDrivesFragment extends Fragment implements PinnedDrivesAdapter.OnPinnedDriveActionListener {

	View rootView;
	PinnedDrivesAdapter adapter;
	private long scrollToDriveId = -1;

	public void setScrollToDriveId(long scrollToDriveId) {
		this.scrollToDriveId = scrollToDriveId;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_pinned_drives, container, false);
		Database.getInstance(getContext()).removePastPinnedDrives();

		// Set the adapter
		RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.list);
		adapter = new PinnedDrivesAdapter(Database.getInstance(getContext()).getPinnedDrives(), this);
		recyclerView.setAdapter(adapter);
		recyclerUpdated();

		return rootView;
	}

	public void recyclerUpdated() {
		rootView.findViewById(R.id.no_pinned_drives).setVisibility(adapter.getItemCount() == 0 ? VISIBLE : GONE);
	}

	@Override
	public void onResume() {
		super.onResume();
		adapter.replaceDataSet(Database.getInstance(getContext()).getPinnedDrives());
		recyclerUpdated();

		if (scrollToDriveId != -1) {
			int index = adapter.getIndex(scrollToDriveId);
			((RecyclerView) rootView.findViewById(R.id.list)).getLayoutManager().scrollToPosition(index);
		}
	}

	@Override
	public void onShare(PinnedDriveCombined drive) {
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);

		String from = Stations.getInstance(getContext()).get(drive.getSchedule().fromId).getLabel();
		String to = Stations.getInstance(getContext()).get(drive.getSchedule().toId).getLabel();

		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
		String text = sharedPref.getString(PREF_SHARE_MESSAGE, "");

		text = text.replace("{" + getString(R.string.field_arrival).toLowerCase() + "}", drive.arrival());
		text = text.replace("{" + getString(R.string.field_departure).toLowerCase() + "}", drive.departure());
		text = text.replace("{" + getString(R.string.field_from).toLowerCase() + "}", from);
		text = text.replace("{" + getString(R.string.field_to).toLowerCase() + "}", to);

		sendIntent.putExtra(Intent.EXTRA_TEXT, text);
		sendIntent.setType("text/plain");

		startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.share_dialog_title)));
	}

	@Override
	public void onFindConnection(PinnedDriveCombined pinnedDrive) {
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		Fragment prev = getFragmentManager().findFragmentByTag("dialogFindConnection");
		if (prev != null) {
			ft.remove(prev);
		}
		ft.addToBackStack(null);

		DialogFragment fragment = FindConnectionDialog.getInstance(pinnedDrive.getSchedule());
		fragment.setTargetFragment(this, AlarmTimePickerDialog.REQUEST_CODE);
		fragment.show(ft, "dialogFindConnection");
	}

	@Override
	public void onAlarmAdd(PinnedDrive pinnedDrive) {
		// Open dialog
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		Fragment prev = getFragmentManager().findFragmentByTag("dialog");
		if (prev != null) {
			ft.remove(prev);
		}
		ft.addToBackStack(null);

		DialogFragment fragment = AlarmTimePickerDialog.getInstance(pinnedDrive.getId());
		fragment.setTargetFragment(this, AlarmTimePickerDialog.REQUEST_CODE);
		fragment.show(ft, "dialog");
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == AlarmTimePickerDialog.REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				PinnedDrive pinnedDrive = Database.getInstance(getContext()).getPinnedDrive(data.getLongExtra(ARG_PINNED_ID, 0));
				boolean alarmSet = pinnedDrive.setAlarmBeforeDeparture(getContext(), data.getIntExtra(ARG_ALARM_MIN, 0));

				if (alarmSet) {
					Database.getInstance(getContext()).save(pinnedDrive);
					adapter.updateItem(pinnedDrive);
				} else {
					Toast.makeText(getContext(), R.string.alarm_not_set, Toast.LENGTH_SHORT).show();
				}
			}
		}
	}

	@Override
	public void onAlarmRemove(PinnedDrive pinnedDrive) {
		pinnedDrive.removeAlarm(getContext());
		Database.getInstance(getContext()).save(pinnedDrive);
	}

	@Override
	public void onDelete(final PinnedDriveCombined combined) {
		combined.first().removeAlarm(getContext());
		List<Integer> deleted = new LinkedList<>();
		for (PinnedDrive drive : combined) {
			int d = Database.getInstance(getContext()).remove(drive.getId());
			if (d > 0)
				deleted.add(d);
		}
		if (deleted.size() > 0) {
			adapter.removeFromDataSet(combined);
			recyclerUpdated();
			Snackbar.make(rootView, R.string.snackbar_drive_removed, Snackbar.LENGTH_LONG).setAction(R.string.action_undo, new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					for (PinnedDrive drive : combined) {
						drive.clearId();
						Database.getInstance(getContext()).save(drive);
					}
					adapter.mergeDataSet(Database.getInstance(getContext()).getPinnedDrives());
					recyclerUpdated();
				}
			}).show();
		}
	}

	public void scrollToId(long id) {
		scrollToDriveId = id;
	}
}
