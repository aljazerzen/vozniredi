package si.erzen.aljaz.vozniredi.activities.schedule;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import si.erzen.aljaz.vozniredi.R;
import si.erzen.aljaz.vozniredi.backend.Drive;
import si.erzen.aljaz.vozniredi.backend.PinnedDriveReminder;
import si.erzen.aljaz.vozniredi.backend.ScheduleDataSetItem;

import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_DURATION;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_OPERATOR;
import static si.erzen.aljaz.vozniredi.util.Constants.FIELD_STRING_IDS;

class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ViewHolder> {
	private static final int[] containerViewIds = new int[]{R.id.field_container_1, R.id.field_container_2, R.id.field_container_3, R.id.field_container_4, R.id.field_container_5, R.id.field_container_6, R.id.field_container_7};

	private static final int TYPE_DRIVE = 1, TYPE_PINNED = 2;

	private List<ScheduleDataSetItem> dataSet;
	private String[] fields;
	private boolean showPin;

	private int selected = -1;

	private OnDriveActionListener listener;

	ScheduleAdapter(String[] fields, OnDriveActionListener listener, boolean showPin) {
		this.fields = fields;
		this.listener = listener;
		this.showPin = showPin;
	}

	void updateDataSet(List<ScheduleDataSetItem> dataSet) {
		this.dataSet = dataSet;
		notifyDataSetChanged();
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		int layout = viewType == TYPE_DRIVE ? R.layout.listitem_drive : R.layout.listitem_drive_reminder_pinned;
		View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);

		return viewType == TYPE_DRIVE
				? new DriveViewHolder(view)
				: new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, int position) {
		if (holder.getItemViewType() == TYPE_DRIVE) {
			boolean activated = position == selected;
			holder.view.setActivated(activated);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				holder.view.setTranslationZ(activated ? 16 : 0);
			}

			for (int i = 0; i < fields.length; i++) {
				LinearLayout ll = (LinearLayout) holder.view.findViewById(containerViewIds[i]);
				ll.setVisibility(i < 3 || activated ? View.VISIBLE : View.GONE);

				// populate fields
				((TextView) ll.getChildAt(0)).setText(FIELD_STRING_IDS.get(fields[i]));

				String fieldText = ((Drive) dataSet.get(position)).get(fields[i]);
				switch (fields[i]) {
					case FIELD_DURATION:
						fieldText = ((Drive) dataSet.get(position)).getDuration(ll.getContext());
						break;
					case FIELD_OPERATOR:
						try {
							int index = Integer.parseInt(fieldText);
							String providers[] = holder.view.getContext().getResources().getStringArray(R.array.providers);
							fieldText = providers[index];
						} catch (NumberFormatException ignored) {
						}
						break;
				}
				if (fieldText != null)
					((TextView) ll.getChildAt(1)).setText(fieldText);
			}
			if (showPin)
				holder.view.findViewById(R.id.button_container).setVisibility(activated ? View.VISIBLE : View.GONE);
		} else {

			PinnedDriveReminder reminder = ((PinnedDriveReminder) dataSet.get(position));
			int stringRes = reminder.isArriving() ? R.string.you_are_arriving : R.string.you_are_departing;
			Context context = holder.view.getContext();

			String text = context.getString(stringRes, reminder.getTime(), reminder.getStationName(context));
			((TextView) holder.view.findViewById(R.id.title)).setText(text);
		}
	}

	@Override
	public int getItemViewType(int position) {
		return dataSet.get(position) instanceof Drive ? TYPE_DRIVE : TYPE_PINNED;
	}

	@Override
	public int getItemCount() {
		return dataSet == null ? 0 : dataSet.size();
	}

	interface OnDriveActionListener {
		void onPin(Drive drive);
	}

	class ViewHolder extends RecyclerView.ViewHolder {
		View view;

		ViewHolder(View view) {
			super(view);
			this.view = view;
		}
	}

	private class DriveViewHolder extends ViewHolder {
		DriveViewHolder(View view) {
			super(view);
			view.setActivated(false);
			view.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					int prevSelected = selected;
					if (selected != getAdapterPosition())
						selected = getAdapterPosition();
					else
						selected = -1;

					if (selected >= 0)
						notifyItemChanged(selected);
					if (prevSelected >= 0)
						notifyItemChanged(prevSelected);
				}
			});

			// Remove unneeded fields
			for (int i = fields.length; i < containerViewIds.length; i++) {
				View toRemove = view.findViewById(containerViewIds[i]);
				((ViewGroup) toRemove.getParent()).removeView(toRemove);
			}
			// Remove pinned button if not used
			if (!showPin) {
				View toRemove = view.findViewById(R.id.button_container);
				((ViewGroup) toRemove.getParent()).removeView(toRemove);
			} else {
				view.findViewById(R.id.button_pin).setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						listener.onPin((Drive) dataSet.get(getAdapterPosition()));
					}
				});
			}
		}
	}
}
