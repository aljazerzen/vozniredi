package si.erzen.aljaz.vozniredi.util;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;

import com.android.vending.billing.IInAppBillingService;

import java.util.ArrayList;

import static si.erzen.aljaz.vozniredi.util.Constants.REQUEST_CODE_BUY_FULL;

@SuppressWarnings("ConstantConditions")
public class BillingHelper {

	private static final String FULL_UPGRADE = "full_upgrade";

	public static void buyFullUpgrade(IInAppBillingService service, Activity context) {
		try {
			Bundle buyIntentBundle = service.getBuyIntent(3, context.getPackageName(), FULL_UPGRADE, "inapp", "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");

			PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");

			context.startIntentSenderForResult(pendingIntent.getIntentSender(),
					REQUEST_CODE_BUY_FULL, new Intent(), 0, 0, 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean hasFullUpgrade(IInAppBillingService service, Context context) {
		Bundle ownedItems = null;
		try {
			ownedItems = service.getPurchases(3, context.getPackageName(), "inapp", null);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}

		int response = ownedItems.getInt("RESPONSE_CODE");
		if (response == 0) {
			ArrayList<String> ownedSkus = ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
			ArrayList<String> purchaseDataList = ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
			ArrayList<String> signatureList = ownedItems.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");

			for (int i = 0; i < purchaseDataList.size(); ++i) {
				String purchaseData = purchaseDataList.get(i);
				String signature = signatureList.get(i);
				String sku = ownedSkus.get(i);

				if (sku.equals(FULL_UPGRADE)) {
					return true;
				}
			}
		}
		return false;
	}

}
