var http = require('http')
var fs = require('fs')

function fetchURL(options, onDone) {
  callback = function(response) {
    var str = ''
    response.on('data', function (chunk) {
      str += chunk
    })
    response.on('end', function () {
      onDone(str)
    })
  }

  http.request(options, callback).end()
}

var optionsIJPP = {
  host: 'www.jpp.si',
  path: '/portalJPP/srvAutoCOmplete?akcija=acIskalnikPostajaliscaPrestopneTocke&modaliteta=0&term=',
  method: 'GET'
}

function fetchIJPP(callback) {
  fetchURL(optionsIJPP, data => {
    var dataJSON = JSON.parse(data)
    var array = []

    for(var i = 0; i < dataJSON.length; i++) {
      if(dataJSON[i].tip == "postaja")
        array.push({
          "name": dataJSON[i].label,
          "id": dataJSON[i].value
        })
    }
    callback(array)
  })
}

function saveArray(array, filename) {
  fs.writeFile(filename, JSON.stringify(array), (err) => {
    if(err)
      return console.log(err)
    console.log("saved to " + filename)
  })
}

fetchIJPP(ijpp => {
  console.log("ijpp: " + ijpp.length)
  saveArray(ijpp, "ijpp.out.json")
})

/*
aplj: 8030
alpetour: 1599
merged: 8773
saved to alpetour.out.json
saved to stations.out.json
*/